<Query Kind="Program">
  <Reference>&lt;RuntimeDirectory&gt;\WPF\PresentationFramework.dll</Reference>
  <Reference>&lt;RuntimeDirectory&gt;\WPF\PresentationCore.dll</Reference>
  <Reference>&lt;RuntimeDirectory&gt;\WPF\WindowsBase.dll</Reference>
  <NuGetReference>Newtonsoft.Json</NuGetReference>
  <Namespace>Newtonsoft.Json</Namespace>
  <Namespace>Newtonsoft.Json.Linq</Namespace>
  <Namespace>System.Windows.Controls</Namespace>
  <Namespace>System.Threading.Tasks</Namespace>
</Query>

void Main()
{
	var panel = new System.Windows.Controls.StackPanel()
	{
		Orientation = System.Windows.Controls.Orientation.Vertical,
	};

	var button = new System.Windows.Controls.Button()
	{
		Content = "Press me to perform a long operation",
		Width = 500,
		Height = 100,
	};

	var label = new Label()
	{
		Content = ".....",
		Width = 100,
		Height = 100,
	};

	panel.Children.Add(button);
	panel.Children.Add(label);


	button.Click += (sender, args) =>
	{
		label.Content = "Work in progress";

		$"Before operation - on thread: {Thread.CurrentThread.ManagedThreadId} - Context: {SynchronizationContext.Current}".Dump();
		LongOperationAsync();
		$"After operation - on thread: {Thread.CurrentThread.ManagedThreadId} - Context: {SynchronizationContext.Current}".Dump();

		label.Content = "Done";
	};

	panel.Dump();
}

void LongOperationAsync()
{
	Thread.Sleep(TimeSpan.FromSeconds(3));
}
