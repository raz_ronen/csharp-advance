<Query Kind="Program">
  <NuGetReference>System.Reactive</NuGetReference>
  <Namespace>System.Reactive</Namespace>
  <Namespace>System.Reactive.Linq</Namespace>
  <Namespace>System.Threading.Tasks</Namespace>
</Query>

void Main()
{
	new Test().CreateAndRunThreads(5);
}

class Test
{
	public void CreateAndRunThreads(int count)
	{
		for (var index = 0; index < count; ++index)
		{
			var data = index;
			new Task(() => Execute(data)).Start();
		}
	}

	public void Execute(object data)
	{
		var value = (int)data;

		for (var index = 0; index < 1000; ++index)
		{
			Thread.Yield();
		}

		$"Thread: {Thread.CurrentThread.ManagedThreadId} has the index of: {value}".Dump();
	}
}