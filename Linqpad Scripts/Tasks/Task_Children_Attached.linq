<Query Kind="Program">
  <NuGetReference>System.Reactive</NuGetReference>
  <Namespace>System.Reactive</Namespace>
  <Namespace>System.Reactive.Linq</Namespace>
  <Namespace>System.Threading.Tasks</Namespace>
</Query>

void Main()
{
	var parent = Task.Factory.StartNew(() =>
	{
		Task.Factory.StartNew(() => Execute("Child 1", TimeSpan.FromSeconds(3)), TaskCreationOptions.AttachedToParent);
		Task.Factory.StartNew(() => Execute("Child 2", TimeSpan.FromSeconds(2)), TaskCreationOptions.AttachedToParent);
	});

	parent.Wait();
	"Parent has finished!".Dump();
}

void Execute(string name, TimeSpan bedtime)
{
	Thread.Sleep(bedtime);
	$"Task {name} has woke up".Dump();
}