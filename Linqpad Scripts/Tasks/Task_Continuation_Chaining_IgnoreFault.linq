<Query Kind="Program">
  <NuGetReference>System.Reactive</NuGetReference>
  <Namespace>System.Reactive</Namespace>
  <Namespace>System.Reactive.Linq</Namespace>
  <Namespace>System.Threading.Tasks</Namespace>
</Query>

void Main()
{
	var task = Task.Run(() => Execute("Task 1", TimeSpan.FromSeconds(1)))
		.ContinueWith(antecedent => Execute("Task 2", TimeSpan.FromSeconds(1)))
		.ContinueWith(antecedent => Execute("Task 3", TimeSpan.FromSeconds(1)))
		.ContinueWith(antecedent => ExecuteAndFail("Task 4", TimeSpan.FromSeconds(1)))
		.ContinueWith(antecedent => Execute("Task 5", TimeSpan.FromSeconds(1)))
		;

	task.Wait();
	$"Task status: {task.Status}".Dump();
}

void ExecuteAndFail(string name, TimeSpan bedtime)
{
	Thread.Sleep(bedtime);
	$"Task {name} has woke up grumpy".Dump();
	throw new Exception("I'm not a morning person!");
}

void Execute(string name, TimeSpan bedtime)
{
	Thread.Sleep(bedtime);
	$"Task {name} has woke up".Dump();
}