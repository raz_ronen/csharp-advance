<Query Kind="Program">
  <NuGetReference>System.Reactive</NuGetReference>
  <Namespace>System.Reactive</Namespace>
  <Namespace>System.Reactive.Linq</Namespace>
  <Namespace>System.Threading.Tasks</Namespace>
  <Namespace>System.Runtime.CompilerServices</Namespace>
</Query>

async void Main()
{
	var asyncOperation = new AsyncOperation<string>(LongOperation, CancellationToken.None);

	"Knock, Knock".Dump();
	"Who's there?".Dump();
	
	var result = await asyncOperation;

	result.Dump();
}

string LongOperation()
{
	Thread.Sleep(TimeSpan.FromSeconds(3));
	return "...Java";
}

public static class Extensions
{
	public static TaskAwaiter<T> GetAwaiter<T>(this AsyncOperation<T> asyncOperation)
	{
		var fakeTask = new TaskCompletionSource<T>();

		asyncOperation.Completed += CompletedCallback;
		asyncOperation.Faulted += FauyltedCallback;

		asyncOperation.Start();

		return fakeTask.Task.GetAwaiter();

		void CompletedCallback(object sender, T result)
		{
			asyncOperation.Completed -= CompletedCallback;
			fakeTask.TrySetResult(result);
		}

		void FauyltedCallback(object sender, Exception exception)
		{
			asyncOperation.Faulted -= FauyltedCallback;
			fakeTask.TrySetException(exception);
		}
	}
}



public class AsyncOperation<T> : IDisposable
{
	public event EventHandler<T> Completed;
	public event EventHandler<Exception> Faulted;

	private readonly CancellationToken _cancellationToken;
	private readonly Func<T> _func;

	public AsyncOperation(Func<T> func, CancellationToken cancellationToken)
		=> (_func, _cancellationToken) = (func, cancellationToken);

	public void Start()
		=> RunAsync();

	private void Execute()
	{
		var result = _func();

		Completed?.Invoke(this, result);
	}

	private void RunAsync()
	{
		_innerThread = new Thread(Execute);
		_innerThread.Start();

		_cancellationToken.Register(() => OnCancelled());

		void OnCancelled()
		{
			_innerThread?.Abort();
			Faulted?.Invoke(this, new TaskCanceledException("Task was cancelled and thread was aborted"));
		}
	}

	public void Dispose()
		=> _innerThread.Abort();

	private Thread _innerThread = null;
}