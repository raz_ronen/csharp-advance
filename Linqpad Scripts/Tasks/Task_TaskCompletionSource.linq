<Query Kind="Program">
  <NuGetReference>System.Reactive</NuGetReference>
  <Namespace>System.Reactive</Namespace>
  <Namespace>System.Reactive.Linq</Namespace>
  <Namespace>System.Threading.Tasks</Namespace>
</Query>

void Main()
{
	var asyncOperation = new AsyncOperation<string>(LongOperation);	
	var task = WrapInTask(asyncOperation);
	
	"Knock, Knock".Dump();
	"Who's there?".Dump();
	
	var result = task.Result;
	
	result.Dump();
}

string LongOperation()
{
	Thread.Sleep(TimeSpan.FromSeconds(3));
	return "...Java";
}

Task<T> WrapInTask<T>(AsyncOperation<T> asyncOperation)
{
	var fakeTask = new TaskCompletionSource<T>();
	
	asyncOperation.Completed += Callback;
	asyncOperation.Start();
	
	return fakeTask.Task;
	
	void Callback(object sender, T result)
	{
		asyncOperation.Completed -= Callback;
		fakeTask.TrySetResult(result);
	}
}

class AsyncOperation<T> : IDisposable
{
	public event EventHandler<T> Completed;
	
	private readonly Func<T> _func;
	
	public AsyncOperation(Func<T> func)
		=> _func = func;
	
	public void Start()
		=> RunAsync();
	
	private void Execute()
	{
		var result = _func();
		
		Completed?.Invoke(this, result);
	}
	
	private void RunAsync()
	{
		_innerThread = new Thread(Execute);
		_innerThread.Start();
	}

	public void Dispose()
		=> _innerThread.Abort();

	private Thread _innerThread = null;
}