<Query Kind="Program">
  <NuGetReference>System.Reactive</NuGetReference>
  <Namespace>System.Reactive</Namespace>
  <Namespace>System.Reactive.Linq</Namespace>
  <Namespace>System.Threading.Tasks</Namespace>
</Query>

async void Main()
{
	await A();
}

async Task A()
{
	await B();
	$"{nameof(A)} is finished".Dump();
}

async Task B()
{
	await C();
	await D();
	
	$"{nameof(B)} is finished".Dump();
}

async Task C()
{
	await Task.Delay(TimeSpan.FromSeconds(1));
	$"{nameof(C)} is finished".Dump();
}

async Task D()
{
	$"		{nameof(D)} Will wait for {nameof(E)} and {nameof(F)}".Dump();
	await Task.WhenAll(F(), E());
	$"{nameof(D)} is finished".Dump();
}

async Task E()
{
	$"		{nameof(E)} Started".Dump();
	await Task.Delay(TimeSpan.FromSeconds(1));
	$"{nameof(E)} is finished".Dump();
}

async Task F()
{
	Thread.Sleep(TimeSpan.FromSeconds(5));

	$"		{nameof(F)} Starts an async operation".Dump();
	await Task.Delay(TimeSpan.FromSeconds(1));
	$"{nameof(F)} is finished".Dump();
}