<Query Kind="Program">
  <NuGetReference>System.Reactive</NuGetReference>
  <Namespace>System.Reactive</Namespace>
  <Namespace>System.Reactive.Linq</Namespace>
  <Namespace>System.Threading.Tasks</Namespace>
  <Namespace>System.Runtime.CompilerServices</Namespace>
</Query>

async void Main()
{
	var result = await new MyTask("Custom Task");
	$"Finished - {result}".Dump();
}

public async MyTask AsyncOperation()
{
	await Task.CompletedTask;
	
	return new MyTask("Custom returned type in an async method");
}

public class MyTask
{
	private readonly string _name;
	
	public MyTask(string name)
		=> _name = name;
	
	public MyCustomAwaiter<string> GetAwaiter()
	{
		return new MyCustomAwaiter<string>(TimeSpan.FromSeconds(3), _name);
	}
}

public class MyCustomAwaiter<T> : ICriticalNotifyCompletion
{
	public bool IsCompleted => false;
		
	private readonly TimeSpan _delay;
	private readonly T _result;

	public MyCustomAwaiter(TimeSpan delay, T result)
		=> (_delay, _result) = (delay, result);
	
	public void OnCompleted(Action continuation)
		=> Task.Delay(_delay).ContinueWith(_ => continuation());

	public T GetResult()
		=> _result;

	public void UnsafeOnCompleted(Action continuation)
		=> OnCompleted(continuation);
}
