<Query Kind="Program">
  <NuGetReference>System.Reactive</NuGetReference>
  <Namespace>System.Reactive</Namespace>
  <Namespace>System.Reactive.Linq</Namespace>
  <Namespace>System.Threading.Tasks</Namespace>
</Query>

void Main()
{
	var tokenSource = new CancellationTokenSource();

	var task = Task.Run(() => LongOperation(tokenSource.Token));

	Task.Delay(TimeSpan.FromSeconds(1.5)).ContinueWith(_ => tokenSource.Cancel());

	task.Wait();
	"Finished!".Dump();
}

void LongOperation(CancellationToken token)
{
	for (var index = 0; index < 10; ++index)
	{
		token.ThrowIfCancellationRequested();
		
		Thread.Sleep(300);
	}
}
