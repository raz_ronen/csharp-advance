<Query Kind="Program">
  <NuGetReference>Newtonsoft.Json</NuGetReference>
  <Namespace>Newtonsoft.Json</Namespace>
  <Namespace>Newtonsoft.Json.Linq</Namespace>
  <Namespace>System.Threading.Tasks</Namespace>
  <Namespace>System.Runtime.CompilerServices</Namespace>
</Query>

async void Main()
{
	"Before waiting....".Dump();
	await 3;
	"Waiting Done!".Dump();
}

public static class Extensions
{
	public static MyCustomAwaiter<int> GetAwaiter(this int value)
	{
		return new MyCustomAwaiter<int>(TimeSpan.FromSeconds(value), value);
	}
}

public class MyCustomAwaiter<T> : ICriticalNotifyCompletion
{
	public bool IsCompleted => false;

	private readonly TimeSpan _delay;
	private readonly T _result;

	public MyCustomAwaiter(TimeSpan delay, T result)
		=> (_delay, _result) = (delay, result);

	public void OnCompleted(Action continuation)
		=> Task.Delay(_delay).ContinueWith(_ => continuation());

	public T GetResult()
		=> _result;

	public void UnsafeOnCompleted(Action continuation)
		=> OnCompleted(continuation);
}
