<Query Kind="Program">
  <Reference>&lt;RuntimeDirectory&gt;\System.Net.Http.dll</Reference>
  <NuGetReference>Newtonsoft.Json</NuGetReference>
  <Namespace>Newtonsoft.Json</Namespace>
  <Namespace>Newtonsoft.Json.Linq</Namespace>
  <Namespace>System.Threading.Tasks</Namespace>
  <Namespace>System.Net.Http</Namespace>
</Query>

async void Main()
{	
	$"Before calling PerformLongOperation()".Dump();
	var result = await PerformLongOperation();
	$"After  calling PerformLongOperation()".Dump();
	
	Environment.NewLine.Dump();
	
	result.Dump();
}

async Task<string> PerformLongOperation()
{
	$"		Before calling ReadGithubHomePage()".Dump();
	var result = await ReadGithubHomePage();
	$"		After  calling ReadGithubHomePage()".Dump();
	
	return result;
}

Task<string> ReadGithubHomePage()
{
	$"				Before calling GetStringAsync()".Dump();
	var result = new HttpClient().GetStringAsync("Https://moaid.codes");
	$"				After  calling GetStringAsync()".Dump();
	
	return result;
}