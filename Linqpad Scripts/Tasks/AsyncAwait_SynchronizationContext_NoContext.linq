<Query Kind="Program">
  <NuGetReference>System.Reactive</NuGetReference>
  <Namespace>System.Reactive</Namespace>
  <Namespace>System.Reactive.Linq</Namespace>
  <Namespace>System.Threading.Tasks</Namespace>
</Query>

async void Main()
{
	for(var index = 0; index < 10; ++index)
	{
		$"Before await: {Thread.CurrentThread.ManagedThreadId}".Dump();
		await Task.Delay(TimeSpan.FromSeconds(1));
		$"	After  await: {Thread.CurrentThread.ManagedThreadId}".Dump();
	}
}