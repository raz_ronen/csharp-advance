<Query Kind="Program">
  <NuGetReference>Newtonsoft.Json</NuGetReference>
  <Namespace>Newtonsoft.Json</Namespace>
  <Namespace>Newtonsoft.Json.Linq</Namespace>
  <Namespace>System.Threading.Tasks</Namespace>
</Query>

async void Main()
{
	var asyncLock = new AsyncLock();
	
	var incrementTask = Task.Run (() => IncrementAsync(asyncLock));
	var decrementTask = Task.Run (() => DecrementAsync(asyncLock));
	
	await Task.WhenAll(incrementTask, decrementTask);
}

public int _sharedResource;

async Task IncrementAsync(AsyncLock asyncLock)
{
	for (var index = 0; index < 10; ++index)
	{
		using (var releaser = await asyncLock.LockAsync())
		{
			_sharedResource++;
			$"Incrementing resouce to {_sharedResource}".Dump();
		}
	}
}

async Task DecrementAsync(AsyncLock asyncLock)
{
	for (var index = 0; index < 10; ++index)
	{
		using (var releaser = await asyncLock.LockAsync())
		{
			_sharedResource--;
			$"Decrementing resouce to {_sharedResource}".Dump();
		}
	}
}

public class AsyncLock
{
	private readonly SemaphoreSlim _semaphore;
	private readonly Task<Releaser> _releaser;
	
	public AsyncLock()
	{
		_semaphore = new SemaphoreSlim(1);
		_releaser = Task.FromResult(new Releaser(this));
	}

	public Task<Releaser> LockAsync()
	{
		var wait = _semaphore.WaitAsync();
		
		return wait.IsCompleted ?
			_releaser :
			wait.ContinueWith((_, state) => new Releaser((AsyncLock)state),	this, CancellationToken.None,
				TaskContinuationOptions.ExecuteSynchronously, TaskScheduler.Default);
	}

	public struct Releaser : IDisposable
	{
		private readonly AsyncLock _toRelease;

		internal Releaser(AsyncLock toRelease)
			=> _toRelease = toRelease;

		public void Dispose()
			=> _toRelease?._semaphore.Release();
	}
}