<Query Kind="Program">
  <NuGetReference>System.Reactive</NuGetReference>
  <Namespace>System.Reactive</Namespace>
  <Namespace>System.Reactive.Linq</Namespace>
  <Namespace>System.Threading.Tasks</Namespace>
</Query>

void Main()
{
	Task.Run (() => Execute("Task 1", TimeSpan.FromSeconds(1)))
		.ContinueWith(antecedent => Execute("Task 2", TimeSpan.FromSeconds(3)))
		.ContinueWith(antecedent => Execute("Task 3", TimeSpan.FromSeconds(1)))
		.ContinueWith(antecedent => Execute("Task 4", TimeSpan.FromSeconds(2)))
		.ContinueWith(antecedent => Execute("Task 5", TimeSpan.FromSeconds(1)))
		.Wait();	
}

void Execute(string name, TimeSpan bedtime)
{
	Thread.Sleep(bedtime);
	$"Task {name} has woke up".Dump();
}