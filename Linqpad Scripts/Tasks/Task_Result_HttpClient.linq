<Query Kind="Program">
  <Reference>&lt;RuntimeDirectory&gt;\System.Net.Http.dll</Reference>
  <NuGetReference>System.Reactive</NuGetReference>
  <Namespace>System.Reactive</Namespace>
  <Namespace>System.Reactive.Linq</Namespace>
  <Namespace>System.Threading.Tasks</Namespace>
  <Namespace>System.Net.Http</Namespace>
</Query>

void Main()
{
	var task = LongOperation();
	
	var result = task.Result;
	
	result.Dump();
}

Task<string> LongOperation()
	=> new HttpClient().GetStringAsync("Https://github.com");