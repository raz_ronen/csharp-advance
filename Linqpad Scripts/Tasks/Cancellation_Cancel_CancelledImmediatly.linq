<Query Kind="Program">
  <NuGetReference>System.Reactive</NuGetReference>
  <Namespace>System.Reactive</Namespace>
  <Namespace>System.Reactive.Linq</Namespace>
  <Namespace>System.Threading.Tasks</Namespace>
</Query>

void Main()
{
	var tokenSource = new CancellationTokenSource();

	tokenSource.Cancel();
	var task = Task.Run(() => LongOperation(tokenSource.Token), tokenSource.Token);

	task.Wait();
	"Finished!".Dump();
}

void LongOperation(CancellationToken token)
{
	for (var index = 0; index < 10; ++index)
	{
		Thread.Sleep(300);
	}

	$"Calculation is done!".Dump();
}
