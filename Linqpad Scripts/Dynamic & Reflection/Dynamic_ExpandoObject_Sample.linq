<Query Kind="Program">
  <NuGetReference>System.Reactive</NuGetReference>
  <Namespace>System.Reactive</Namespace>
  <Namespace>System.Reactive.Linq</Namespace>
  <Namespace>System.Dynamic</Namespace>
</Query>

void Main()
{
	dynamic obj = new ExpandoObject();
	
	IDictionary<string, object> i = obj;
	
	obj.Foo = 10;
	obj.Bar = 20;
	
	obj.A = "Moaid";
	
	Console.WriteLine(obj.Foo);	
	Console.WriteLine(obj.Bar);
	Console.WriteLine(obj.A);
	
	i.ToList().Dump();	
}

// Define other methods and classes here
