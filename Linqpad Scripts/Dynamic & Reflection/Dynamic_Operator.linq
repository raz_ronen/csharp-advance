<Query Kind="Program">
  <NuGetReference>System.Reactive</NuGetReference>
  <Namespace>System.Reactive</Namespace>
  <Namespace>System.Reactive.Linq</Namespace>
</Query>

void Main()
{
	Console.WriteLine(Sum(2, 33, 22, 30)); 
	
	List<string> cities = new List<string> { "New York", "Tokyo", "Paris", "London" };
	
	Console.WriteLine(Sum(cities.ToArray()));
	
	var vectors = new []{ new Vector(1, 2, 3), new Vector(3, 2, 1) };
	
	Console.WriteLine(Sum(vectors));
}

public static dynamic Sum(params dynamic[] values)
{
	dynamic sum = values[0];

	for (int i = 1; i < values.Length; i++)
	{
		sum += values[i];
	}

	return sum;
}

public class Vector
{
	public int X { get; }
	public int Y { get; }
	public int Z { get; }
	
	public Vector(int x, int y, int z)
		=> (X, Y, Z) = (x, y, z);

	public override string ToString()
		=> $"[X: {X}, Y: {Y}, Z: {Z}]";
		
	public static Vector operator +(Vector a, Vector b)
		=> new Vector(a.X + b.X, a.Y + b.Y, a.Z + b.Z);
}