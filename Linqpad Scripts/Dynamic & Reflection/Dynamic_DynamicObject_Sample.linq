<Query Kind="Program">
  <NuGetReference>System.Reactive</NuGetReference>
  <Namespace>System.Reactive</Namespace>
  <Namespace>System.Reactive.Linq</Namespace>
  <Namespace>System.Dynamic</Namespace>
</Query>

void Main()
{
	dynamic obj = new MyDynamicObject();
	
	Console.WriteLine(obj.Marco);
	Console.WriteLine(obj.GenerateRandomNumber());	
	
	obj.Company = "CodeValue";
	
	Console.WriteLine(obj.Company);
}

class MyDynamicObject : DynamicObject
{
	private Dictionary<string, object> _map = new Dictionary<string, object>();
	
	public override bool TryGetMember(GetMemberBinder binder, out object result)
	{
		if(binder.Name == "Marco")
		{
			result = "Polo";
			return true;
		}
		
		return _map.TryGetValue(binder.Name, out result);
	}

	public override bool TrySetMember(SetMemberBinder binder, object value)
	{
		_map[binder.Name] = value;
		return true;
	}

	public override bool TryInvokeMember(InvokeMemberBinder binder, object[] args, out object result)
	{
		result = "42";
		return true;
	}
}
