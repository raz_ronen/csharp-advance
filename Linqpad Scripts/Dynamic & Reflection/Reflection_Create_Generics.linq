<Query Kind="Program">
  <NuGetReference>Newtonsoft.Json</NuGetReference>
  <Namespace>Newtonsoft.Json</Namespace>
  <Namespace>Newtonsoft.Json.Linq</Namespace>
</Query>

void Main()
{
	Type openType = typeof(Dictionary<,>);

	Type[] typeArgs = { typeof(string), typeof(string) };

	Type closedType = openType.MakeGenericType(typeArgs);
	
	var dictionary = (Dictionary<string, string>)Activator.CreateInstance(closedType);
	
	dictionary["Foo"] = "Bar";
	
	Console.WriteLine(dictionary["Foo"]);
}
