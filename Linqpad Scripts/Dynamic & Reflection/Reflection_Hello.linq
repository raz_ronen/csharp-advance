<Query Kind="Program">
  <NuGetReference>System.Reactive</NuGetReference>
  <Namespace>System.Reactive</Namespace>
  <Namespace>System.Reactive.Linq</Namespace>
</Query>

void Main()
{
	Invoke(new A(), "Moaid");
	Invoke(new B(), "Hathot");
	Invoke(new C(), "Microsoft");
}

static void Invoke(object obj, string s)
{
	obj.GetType().GetMethod("Hello").Invoke(obj, new[] { s}).Dump();
}

public class A
{
	public string Hello(string s)
	{
		return "A: " + s;
	}
}

public class B
{
	public string Hello(string s)
	{
		return "B: " + s;
	}
}

public class C
{
	public string Hello(string s)
	{
		return "C: " + s;
	}
}