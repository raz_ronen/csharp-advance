<Query Kind="Program">
  <NuGetReference>System.Reactive</NuGetReference>
  <Namespace>System.Reactive</Namespace>
  <Namespace>System.Reactive.Linq</Namespace>
  <Namespace>System.Dynamic</Namespace>
</Query>


void Main()
{
	dynamic planets = DynamicXElement.Create(XElement.Parse(GetXmlElements()));

	var mercury = planets.Planet;
	Console.WriteLine(mercury);
	
	var venus = planets["Planet", 1];
	Console.WriteLine(venus);
	
	var ourMoon = planets["Planet", 2].Moons.Moon;
	Console.WriteLine(ourMoon);

	var marsMoon = planets["Planet", 3]["Moons", 0].Moon;
	Console.WriteLine(marsMoon);
}

public class DynamicXElement : DynamicObject
{
	private readonly XElement _element;
	
	private DynamicXElement(XElement element)
		=> _element = element;
		
	public static DynamicXElement Create(XElement element)
		=> new DynamicXElement(element);

	public override bool TryGetMember(GetMemberBinder binder, out object result)
	{
		var element = _element
			.Descendants(binder.Name)
			.FirstOrDefault();
			
		result = new DynamicXElement(element);

		return result != null;
	}
	public override bool TryGetIndex(GetIndexBinder binder, object[] indexes, out object result)
	{
		if(2 != indexes.Length || !int.TryParse(indexes[1].ToString(), out var index))
		{
			result = null;
			return false;
		}

		var element = _element
			.Descendants(indexes[0].ToString())
			.Skip(index)
			.FirstOrDefault();
			
		result = new DynamicXElement(element);

		return null != result;
	}

	public override string ToString()
		=> _element.Value;
}

string GetXmlElements()
{
	return @"<Planets>
  <Planet>
    <Name>Mercury</Name>
  </Planet>
  <Planet>
    <Name>Venus</Name>
  </Planet>
  <Planet>
    <Name>Earth</Name>
    <Moons>
      <Moon>Moon</Moon>
    </Moons>
  </Planet>
  <Planet>
    <Name>Mars</Name>
    <Moons>
      <Moon>Phobos</Moon>
      <Moon>Deimos</Moon>
    </Moons>
  </Planet>
</Planets>
";
}