<Query Kind="Program">
  <NuGetReference>System.Reactive</NuGetReference>
  <Namespace>System.Reactive</Namespace>
  <Namespace>System.Reactive.Linq</Namespace>
</Query>

void Main()
{

	My target = null;
	LazyInitializer.EnsureInitialized<My>(ref target, () => new My(3));
	LazyInitializer.EnsureInitialized<My>(ref target, () => new My(4));
	LazyInitializer.EnsureInitialized<My>(ref target, () => new My(5));
}

public class My
{
	public My(int i)
	{
		$"Being Initialized: {i}".Dump();
	}
}
