<Query Kind="Program">
  <NuGetReference>Newtonsoft.Json</NuGetReference>
  <Namespace>Newtonsoft.Json</Namespace>
  <Namespace>Newtonsoft.Json.Linq</Namespace>
</Query>

void Main()
{
	IEnumerable<Base> a = new List<Derived>();
	IEnumerable<Base> b = new Derived[]{ new Derived() };
	Base[] c = new Derived[]{ new Derived() };
}

class Base
{
	
}

class Derived : Base
{
	
}
