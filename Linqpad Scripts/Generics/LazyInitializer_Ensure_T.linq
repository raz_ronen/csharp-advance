<Query Kind="Program">
  <NuGetReference>System.Reactive</NuGetReference>
  <Namespace>System.Reactive</Namespace>
  <Namespace>System.Reactive.Linq</Namespace>
</Query>

void Main()
{

	My target = null;
	LazyInitializer.EnsureInitialized<My>(ref target);
	LazyInitializer.EnsureInitialized<My>(ref target);
	LazyInitializer.EnsureInitialized<My>(ref target);
}

public class My
{
	public My()
	{
		"Being Initialized".Dump();
	}
}
