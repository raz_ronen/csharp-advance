<Query Kind="Program">
  <NuGetReference>System.Reactive</NuGetReference>
  <Namespace>System.Reactive</Namespace>
  <Namespace>System.Reactive.Linq</Namespace>
</Query>

void Main()
{
	IG<Base> b = new G<Derived>();
}

interface IG<out T>
{
	
}

class G<T> : IG<T>
{
	
}

class Base
{
	
}

class Derived : Base
{
	
}
