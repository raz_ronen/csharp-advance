<Query Kind="Program">
  <NuGetReference>Newtonsoft.Json</NuGetReference>
  <Namespace>Newtonsoft.Json</Namespace>
  <Namespace>Newtonsoft.Json.Linq</Namespace>
</Query>

void Main()
{
	MyDelegate f1 = new MyDelegate(FooBase);
	Action<Derived> f2 = FooBase;
}

void FooDerived(Derived derived)
{
	
}

void FooBase(Base b)
{
	
}

delegate void MyDelegate(Derived derived);

class Base
{
	
}

class Derived : Base
{
	
}
