<Query Kind="Program">
  <NuGetReference>System.Reactive</NuGetReference>
  <Namespace>System.Reactive</Namespace>
  <Namespace>System.Reactive.Linq</Namespace>
</Query>

void Main()
{
	Lazy<My> lazy = new Lazy<My>(() => new My(10));
	
	lazy.IsValueCreated.Dump();
	var _ = lazy.Value;
	lazy.IsValueCreated.Dump();
	_ = lazy.Value;
	_ = lazy.Value;
	_ = lazy.Value;
	_ = lazy.Value;
	_ = lazy.Value;
	_ = lazy.Value;
	_ = lazy.Value;
	_ = lazy.Value;
	
	lazy.Value.Dump();
}

public class My
{
	private int _i;

	public My(int i)
	{
		_i = i;
		$"Being Initialized: {i}".Dump();
	}

	public override string ToString()
		=> $"My value is: {_i}";
}
