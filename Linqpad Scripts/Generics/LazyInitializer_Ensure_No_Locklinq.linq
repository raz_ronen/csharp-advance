<Query Kind="Program">
  <NuGetReference>System.Reactive</NuGetReference>
  <Namespace>System.Reactive</Namespace>
  <Namespace>System.Reactive.Linq</Namespace>
  <Namespace>System.Threading.Tasks</Namespace>
</Query>

void Main()
{

	My target = null;
	
	Parallel.For(0, 10, i => LazyInitializer.EnsureInitialized<My>(ref target, () => new My(i)));
	
	LazyInitializer.EnsureInitialized<My>(ref target, () => new My(15));
	target?.Dump();
}

public class My
{
	private int _i;
	
	public My(int i)
	{
		_i = i;
		$"Being Initialized: {i}".Dump();
	}

	public override string ToString()
		=> $"My value is: {_i}";
}
