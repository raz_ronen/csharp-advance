<Query Kind="Program">
  <NuGetReference>System.Reactive</NuGetReference>
  <Namespace>System.Reactive</Namespace>
  <Namespace>System.Reactive.Linq</Namespace>
</Query>

void Main()
{
	IG<Derived> d = new G<Base>();
}

interface IG<in T>
{
	
}

class G<T> : IG<T>
{
	
}

class Base
{
	
}

class Derived : Base
{
	
}
