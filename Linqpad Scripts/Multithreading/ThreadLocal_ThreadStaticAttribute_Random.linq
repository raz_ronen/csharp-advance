<Query Kind="Program">
  <NuGetReference>System.Reactive</NuGetReference>
  <Namespace>System.Reactive</Namespace>
  <Namespace>System.Reactive.Linq</Namespace>
  <Namespace>System.Threading.Tasks</Namespace>
</Query>

void Main()
{
	new Test().CreateAndRunThreads(5);
}

class Test
{
	[ThreadStatic]
	private static int _threadLocalSum;
	
	private readonly Random _sharedRandom = new Random();
	private readonly object _lock = new object();
		
	public void CreateAndRunThreads(int count)
	{
		for(var index = 0; index < count; ++index)
		{
			new Thread(Execute).Start();
		}
	}
	
	public void Execute()
	{
		for(var index = 0; index < 1000; ++index)
		{
			lock(_lock)
			{
				_threadLocalSum += _sharedRandom.Next();
			}
		}

		$"Thread: {Thread.CurrentThread.ManagedThreadId} has the sum of: {_threadLocalSum}".Dump();
	}
}