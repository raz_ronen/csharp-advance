<Query Kind="Program">
  <NuGetReference>System.Reactive</NuGetReference>
  <Namespace>System.Reactive</Namespace>
  <Namespace>System.Reactive.Linq</Namespace>
  <Namespace>System.Threading.Tasks</Namespace>
</Query>

void Main()
{
	var marcoHandle = new EventWaitHandle(true, EventResetMode.ManualReset);
	var poloHandle = new EventWaitHandle(false, EventResetMode.ManualReset);

	var marco = new Thread(SayMarco);
	var polo = new Thread(SayMarco);

	marco.Start(new Data { Message = "Marco", Count = 3, WaitHandle = marcoHandle, Signalhandle = poloHandle });
	//Should sleep here?
	polo.Start(new Data { Message = "Polo", Count = 3, WaitHandle = poloHandle, Signalhandle = marcoHandle });
}

public static void SayMarco(object args)
{
	var (message, count, waitHandle, signalHandle) = (args as Data);

	for (var index = 0; index < count; ++index)
	{
		waitHandle.WaitOne();
		waitHandle.Reset();
		
		message.Dump();
		Thread.Sleep(TimeSpan.FromSeconds(1));
		
		signalHandle.Set();
	}
}

public class Data
{
	public string Message { get; set; }
	public EventWaitHandle WaitHandle { get; set; }
	public EventWaitHandle Signalhandle { get; set; }
	public int Count { get; set; }

	public void Deconstruct(out string message, out int count,  out EventWaitHandle waitHandle, out EventWaitHandle signalHandle)
		=> (message, count, waitHandle, signalHandle) = (Message, Count, WaitHandle, Signalhandle);
}