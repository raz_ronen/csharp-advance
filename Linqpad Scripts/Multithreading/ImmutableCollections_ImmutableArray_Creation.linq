<Query Kind="Program">
  <NuGetReference>System.Collections.Immutable</NuGetReference>
  <NuGetReference>System.Reactive</NuGetReference>
  <Namespace>System.Reactive</Namespace>
  <Namespace>System.Reactive.Linq</Namespace>
  <Namespace>System.Threading.Tasks</Namespace>
  <Namespace>System.Collections.Immutable</Namespace>
</Query>

void Main()
{
	var arr = ToImmutable();
	
	var newArr = arr.Add(10).Add(3);
	
	arr.Dump();
	newArr.Dump();
}

ImmutableArray<int> ToImmutable()
{
	return new int[]{ 1, 2, 3}.ToImmutableArray();
}

void Create()
{
	var array = ImmutableArray.Create(1, 2, 3);

	array.Dump();
}

void Builder()
{
	var builder = ImmutableArray.CreateBuilder<int>();

	builder.Add(1);
	builder.Add(2);
	builder.Add(3);

	builder.ToImmutableArray().Dump();
}