<Query Kind="Program">
  <NuGetReference>System.Reactive</NuGetReference>
  <Namespace>System.Reactive</Namespace>
  <Namespace>System.Reactive.Linq</Namespace>
  <Namespace>System.Threading.Tasks</Namespace>
</Query>

void Main()
{
	new Test().CreateAndRunThreads(5);
}

class Test
{		
	public void CreateAndRunThreads(int count)
	{
		for(var index = 0; index < count; ++index)
		{
			new Thread(Execute).Start(index);
		}
	}
	
	public void Execute(object data)
	{
		var value = (int)data;

		var slot = Thread.AllocateDataSlot();
		for(var index = 0; index < 1000; ++index)
		{
			Thread.SetData(slot, value);			
		}

		$"Thread: {Thread.CurrentThread.ManagedThreadId} has the sum of: {Thread.GetData(slot)}".Dump();
	}
}