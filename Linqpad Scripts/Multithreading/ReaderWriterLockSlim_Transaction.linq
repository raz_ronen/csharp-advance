<Query Kind="Program">
  <NuGetReference>System.Reactive</NuGetReference>
  <Namespace>System.Reactive</Namespace>
  <Namespace>System.Reactive.Linq</Namespace>
  <Namespace>System.Threading.Tasks</Namespace>
</Query>

void Main()
{
	var transaction = new Transaction();

	Console.WriteLine(transaction.LastTransaction);
	transaction.PerformTransaction();
	Console.WriteLine(transaction.LastTransaction);
}

class Transaction : IDisposable
{
	private readonly ReaderWriterLockSlim _lock = new ReaderWriterLockSlim(LockRecursionPolicy.NoRecursion);

	private DateTime _timeOfLastTrans;

	public void PerformTransaction()
	{
		_lock.EnterWriteLock();
		PerformTransactionCode();
		_timeOfLastTrans = DateTime.Now;
		_lock.ExitWriteLock();
	}

	public DateTime LastTransaction
	{
		get
		{
			_lock.EnterReadLock();
			// This code has shared access to the data...
			DateTime temp = _timeOfLastTrans;
			_lock.ExitReadLock();
			return temp;
		}
	}

	private void PerformTransactionCode()
	{
		"Performing Transaction".Dump();
		Thread.Sleep(TimeSpan.FromSeconds(1));
	}

	public void Dispose() 
		=> _lock.Dispose();
}