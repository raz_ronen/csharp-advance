<Query Kind="Program">
  <NuGetReference>System.Reactive</NuGetReference>
  <Namespace>System.Reactive</Namespace>
  <Namespace>System.Reactive.Linq</Namespace>
  <Namespace>System.Threading.Tasks</Namespace>
</Query>

void Main()
{
	var t = new Thread(DoWork);
	// var t = new Thread(new ParameterizedThreadStart(DoWork));
	
	var data = "Some data";
	t.Start(data);
}

static void DoWork(object data) 
{
	Console.WriteLine($"Started working... {data}");
	Thread.Sleep(2000);
	Console.WriteLine("Completed.");
}