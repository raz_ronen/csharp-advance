<Query Kind="Program">
  <NuGetReference>System.Reactive</NuGetReference>
  <Namespace>System.Reactive</Namespace>
  <Namespace>System.Reactive.Linq</Namespace>
  <Namespace>System.Threading.Tasks</Namespace>
  <Namespace>System.Collections.Concurrent</Namespace>
</Query>

void Main()
{
	var dictionary = new ConcurrentDictionary<string, int>();
	
	dictionary.GetOrAdd("MS", key => 0);
	dictionary.AddOrUpdate("MS", key => 0, (key, oldValue) => oldValue + 1);
	dictionary.GetOrAdd("MS", key => 0);
	
	dictionary.Dump();
}