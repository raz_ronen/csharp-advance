<Query Kind="Program">
  <NuGetReference>System.Reactive</NuGetReference>
  <Namespace>System.Reactive</Namespace>
  <Namespace>System.Reactive.Linq</Namespace>
  <Namespace>System.Threading.Tasks</Namespace>
</Query>

void Main()
{
	new Test().CreateAndRunThreads(5);
}

class Test
{
	public void CreateAndRunThreads(int count)
	{
		ThreadLocal<string> threadLocal = new ThreadLocal<string>();
		threadLocal.Value = "Moaid";

		var threads = new List<Thread>();
		for (var index = 0; index < count; ++index)
		{
			var thread = new Thread(Execute);
			threads.Add(thread);
			thread.Start(threadLocal);
		}

		foreach (var thread in threads)
		{
			thread.Join();
		}

		$"Final thread data: {threadLocal.Value}".Dump();
	}

	public void Execute(object data)
	{
		var slot = (ThreadLocal<string>)data;

		for (var index = 0; index < 1000; ++index)
		{
			slot.Value = Thread.CurrentThread.ManagedThreadId.ToString();
		}

		$"Thread: {Thread.CurrentThread.ManagedThreadId} has the sum of: {slot.Value}".Dump();
	}
}