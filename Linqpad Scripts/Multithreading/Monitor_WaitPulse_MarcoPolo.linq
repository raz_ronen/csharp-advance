<Query Kind="Program">
  <NuGetReference>System.Reactive</NuGetReference>
  <Namespace>System.Reactive</Namespace>
  <Namespace>System.Reactive.Linq</Namespace>
  <Namespace>System.Threading.Tasks</Namespace>
  <Namespace>System</Namespace>
</Query>

void Main()
{
	var syncLock = new object();
	
	var marco = new Thread(SayMarco);
	var polo = new Thread(SayMarco);
	
	marco.Start(new Data { Lock = syncLock, Message = "Marco", Count = 3 });
	//Should sleep here?
	polo.Start(new Data { Lock = syncLock, Message = "Polo", Count = 3 });
}

public static void SayMarco(object args)
{
	var (syncLock, message, count) = (args as Data);

	Monitor.Enter(syncLock);
	{
		for(var index = 0; index < count; ++index)
		{
			message.Dump();			
			Thread.Sleep(TimeSpan.FromSeconds(1));
			
			Monitor.Pulse(syncLock);		
			
			Monitor.Wait(syncLock);
		}		
	}
	Monitor.Exit(syncLock);
}

public class Data
{
	public string Message { get; set; }
	public object Lock { get; set; }
	public int Count {get; set; }

	public void Deconstruct(out object syncLock, out string message, out int count)
		=> (syncLock, message, count) = (Lock, Message, Count);
}