<Query Kind="Program">
  <NuGetReference>System.Reactive</NuGetReference>
  <Namespace>System.Reactive</Namespace>
  <Namespace>System.Reactive.Linq</Namespace>
  <Namespace>System.Threading.Tasks</Namespace>
</Query>

void Main()
{
	var transaction = new Transaction();
	
	Console.WriteLine(transaction.LastTransaction);
	transaction.PerformTransaction();
	Console.WriteLine(transaction.LastTransaction);
}

internal sealed class Transaction
{
	private DateTime _timeOfLastTransaction;               // Field indicating the time of the last transaction performed
	private readonly object _syncLock = new object();    // The object that is used as a lock

	public void PerformTransaction()
	{
		Monitor.Enter(_syncLock);                                  // Acquire the lock
		PerformTransactionCode();                                 // Perform the transaction
		_timeOfLastTransaction = DateTime.Now;        // Update the transaction’s timestamp.
		Monitor.Exit(_syncLock);                                    // Release the lock
	}

	// Read-only property returning the time of the last transaction
	public DateTime LastTransaction
	{
		get
		{
			Monitor.Enter(_syncLock);                                     // Acquire the lock
			DateTime dt = _timeOfLastTransaction;            // Save the time of the last transaction
			Monitor.Exit(_syncLock);                                       // Release the lock
			return dt;                                                              // Return the saved date/time
		}
	}
	
	private void PerformTransactionCode()
	{
		"Performing Transaction".Dump();
		Thread.Sleep(TimeSpan.FromSeconds(1));
	}
}
