<Query Kind="Program">
  <NuGetReference>System.Reactive</NuGetReference>
  <Namespace>System.Reactive</Namespace>
  <Namespace>System.Reactive.Linq</Namespace>
  <Namespace>System.Threading.Tasks</Namespace>
</Query>

void Main()
{
	new Test().CreateAndRunThreads(5);
}

class Test
{
	public void CreateAndRunThreads(int count)
	{
		var slot = Thread.AllocateDataSlot();
		Thread.SetData(slot, "moaid");
		
		var threads = new List<Thread>();
		for (var index = 0; index < count; ++index)
		{
			var thread = new Thread(Execute);			
			threads.Add(thread);
			thread.Start(slot);
		}
		
		foreach(var thread in threads)
		{
			thread.Join();
		}

		$"Final thread data: {Thread.GetData(slot)}".Dump();
	}

	public void Execute(object data)
	{
		var slot = (LocalDataStoreSlot)data;

		for (var index = 0; index < 1000; ++index)
		{
			Thread.SetData(slot, Thread.CurrentThread.ManagedThreadId);
		}

		$"Thread: {Thread.CurrentThread.ManagedThreadId} has the sum of: {Thread.GetData(slot)}".Dump();
	}
}