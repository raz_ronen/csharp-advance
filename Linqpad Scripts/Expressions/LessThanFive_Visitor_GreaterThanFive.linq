<Query Kind="Program">
  <NuGetReference>System.Reactive</NuGetReference>
  <Namespace>System.Reactive</Namespace>
  <Namespace>System.Reactive.Linq</Namespace>
</Query>

void Main()
{
	ParameterExpression numParam = Expression.Parameter(typeof(int), "num");
	ConstantExpression five = Expression.Constant(5, typeof(int));
	BinaryExpression numLessThanFive = Expression.LessThan(numParam, five);
	BinaryExpression numGreaterThanFive = (BinaryExpression)new BiggerThanModifier().Modify(numLessThanFive);

	Expression<Func<int, bool>> lessThanLambda = Expression.Lambda<Func<int, bool>>(numLessThanFive, new ParameterExpression[] { numParam });
	Expression<Func<int, bool>> greaterThanLambda = Expression.Lambda<Func<int, bool>>(numGreaterThanFive, new ParameterExpression[] { numParam });
	
	var lessFunc = lessThanLambda.Compile();
	var greatFunc = greaterThanLambda.Compile();
	
	lessFunc(3).Dump();
	greatFunc(3).Dump();
	
	numLessThanFive.Dump();
	numGreaterThanFive.Dump();
}

public class BiggerThanModifier : ExpressionVisitor
{
	public Expression Modify(Expression expression)
	{
		return Visit(expression);
	}

	protected override Expression VisitBinary(BinaryExpression node)
	{
		if(node.NodeType != ExpressionType.LessThan)
		{
			return base.Visit(node);
		}
		
		var left = Visit(node.Left);
		var right = Visit(node.Right);
		
//		return Expression.MakeBinary(ExpressionType.GreaterThan, left, right);
		return Expression.GreaterThan(left, right);
	}
}