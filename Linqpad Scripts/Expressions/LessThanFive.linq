<Query Kind="Program">
  <NuGetReference>System.Reactive</NuGetReference>
  <Namespace>System.Reactive</Namespace>
  <Namespace>System.Reactive.Linq</Namespace>
</Query>

void Main()
{
	ParameterExpression numParam = Expression.Parameter(typeof(int), "num");
	ConstantExpression five = Expression.Constant(5, typeof(int));
	BinaryExpression numLessThanFive = Expression.LessThan(numParam, five);

	Expression<Func<int, bool>> lambda = Expression.Lambda<Func<int, bool>>(numLessThanFive, new ParameterExpression[] { numParam });
	
	var func = lambda.Compile();
	
	func(3).Dump();
	
//	lambda.Dump();
}