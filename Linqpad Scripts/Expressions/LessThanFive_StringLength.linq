<Query Kind="Program">
  <NuGetReference>Newtonsoft.Json</NuGetReference>
  <Namespace>Newtonsoft.Json</Namespace>
  <Namespace>Newtonsoft.Json.Linq</Namespace>
  <Namespace>System.Threading.Tasks</Namespace>
</Query>

void Main()
{
	Expression<Func<string, bool>> greaterThan = str => str.Length > 5;
	
	var lessThan = (Expression<Func<string, bool>>)new LessThanModifier().Modify(greaterThan);
	
	var greaterThanFunc = greaterThan.Compile();
	var lessThanFunc = lessThan.Compile();
	
	greaterThanFunc("Microsoft").Dump();
	lessThanFunc("Microsoft").Dump();

	greaterThan.Dump();
	lessThan.Dump();
}

public class LessThanModifier : ExpressionVisitor
{
	public Expression Modify(Expression expression)
	{
		return Visit(expression);
	}

	protected override Expression VisitBinary(BinaryExpression node)
	{
		if (node.NodeType != ExpressionType.GreaterThan)
		{
			return base.Visit(node);
		}

		var left = Visit(node.Left);
		var right = Visit(node.Right);

		//		return Expression.MakeBinary(ExpressionType.LessThan, left, right);
		return Expression.LessThan(left, right);
	}
}
