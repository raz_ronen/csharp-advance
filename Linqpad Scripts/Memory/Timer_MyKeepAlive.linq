<Query Kind="Program">
  <NuGetReference>System.Reactive</NuGetReference>
  <Namespace>System.Reactive</Namespace>
  <Namespace>System.Reactive.Linq</Namespace>
  <Namespace>System.Runtime.CompilerServices</Namespace>
</Query>

void Main()
{
	// Create a Timer object that knows to call our TimerCallback
	// method once every 2000 milliseconds.
	Timer t = new Timer(TimerCallback, null, 0, 2000);

	// Wait for the user to hit <Enter>.
	Console.ReadLine();
	MyKeepAlive(t);
}

[MethodImpl(MethodImplOptions.NoInlining)]
static void MyKeepAlive(object obj)
{
	
}

private static void TimerCallback(Object o)
{
	// Display the date/time when this method got called.
	Console.WriteLine("In TimerCallback: " + DateTime.Now);
	// Force a garbage collection to occur for this demo.
	GC.Collect();
}