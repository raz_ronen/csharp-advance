<Query Kind="Program">
  <NuGetReference>System.Reactive</NuGetReference>
  <Namespace>System.Reactive</Namespace>
  <Namespace>System.Reactive.Linq</Namespace>
</Query>

void Main()
{
	var pub = new Publisher<string>();
	var sub = new Subscriber<string>();
	
	pub.Register(sub);
	pub.Publish(".Net is fun!");
	sub = null;
	
	pub.Publish("So is C#");
	GC.Collect();

	pub.Publish("Kawabanga");
}

class Subscriber<T> : INotify<T>
{
	public void Notify(T data)
	{
		Console.WriteLine("\tReceived notification: {0}", data);
	}
}

interface INotify<T>
{ 
	void Notify(T data); 
}

class Publisher<T> where T: class
{
	WeakReference<INotify<T>> _client;
	
	public void Register(INotify<T> client) 
		=> _client = new WeakReference<INotify<T>>(client);

	public void Publish(T data)
	{
		Console.WriteLine("Publishing... data = {0}", data);

		if(_client.TryGetTarget(out var notify))
		{
			notify.Notify(data);
		}
		else
		{
			_client = null;	
		}
	}
}
