<Query Kind="Program">
  <NuGetReference>System.Reactive</NuGetReference>
  <Namespace>System.Reactive</Namespace>
  <Namespace>System.Reactive.Linq</Namespace>
</Query>

void Main()
{
	var pub = new Publisher();
	var sub = new Subscriber();
	
	pub.Register(sub);
	pub.Publish(".Net is fun!");
	sub = null;
	
	pub.Publish("So is C#");
	GC.Collect();

	pub.Publish("Kawabanga");
}

class Subscriber : INotify
{
	public void Notify(object data)
	{
		Console.WriteLine("\tReceived notification: {0}", data.ToString());
	}
}


interface INotify
{ 
	void Notify(object data); 
}

class Publisher
{
	WeakReference _client;
	
	public void Register(INotify client) 
		=> _client = new WeakReference(client);

	public void Publish(object data)
	{
		Console.WriteLine("Publishing... data = {0}", data);

		var notify = _client.Target as INotify;
		if (notify != null)
		{
			notify.Notify(data);
		}
		else
		{
			_client = null;
		}
	}
}
