<Query Kind="Program">
  <Reference>&lt;RuntimeDirectory&gt;\System.Activities.dll</Reference>
  <NuGetReference>System.Reactive</NuGetReference>
  <Namespace>System.Reactive</Namespace>
  <Namespace>System.Reactive.Linq</Namespace>
  <Namespace>System.Activities.Statements</Namespace>
</Query>

void Main()
{
	// number 3 from beginning
	Index i1 = 3;
	
	// number 4 from end
	Index i2 = ^4;
	
	//1, 2, 3
	Range r1 = 1..4;
	
	//0, 1, 2, 3, 4
	Range r2 = ..5;
	
	//5, 6, 7, 8, 9
	Range r3 = 5..;
	
	//5, 6, 7, 8
	Range r4 = 5..^1;

	//7
	Range r5 = 7..^2;
	
	//Empty
	Range r6 = 7..^3;	
	
	//OverflowException
	Range r7 = 7..^4;
	
	//3, 4, 5
	Range r8 = i1..i2;	
	
	//All
	Range r9 = ..;
	
	int[] arr = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
	
	arr[i1].Dump("Index: 3");	
	arr[r1].Dump("Range: 3");
	arr[^4].Dump("^4");
	arr[1..4].Dump("1..4");
	arr[..5].Dump("..5");
	arr[5..].Dump("5..");
	arr[5..^1].Dump("5..^1");
	arr[7..^2].Dump("7..^2");
	arr[7..^3].Dump("7..^3");
	arr[i1..i2].Dump("i1..i2");
	arr[r9].Dump("..");
	arr[7..^4].Dump("7..^4");
}