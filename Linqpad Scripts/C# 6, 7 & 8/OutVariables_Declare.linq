<Query Kind="Program">
  <NuGetReference>System.Reactive</NuGetReference>
  <Namespace>System.Reactive</Namespace>
  <Namespace>System.Reactive.Linq</Namespace>
</Query>

void Main()
{
	void ReturnManyValues(out int x, out int y, out string z)
	{
		x = 10; y = 20; z = "Hello";
	}
	
	ReturnManyValues(out int a, out int b, out var s); //the compiler resolve s’s type
	
	Console.WriteLine($"a:{a}, b:{b}, s:{s}");
}

// Define other methods and classes here
