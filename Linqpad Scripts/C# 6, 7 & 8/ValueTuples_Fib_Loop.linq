<Query Kind="Program">
  <NuGetReference>System.Reactive</NuGetReference>
  <Namespace>System.Reactive</Namespace>
  <Namespace>System.Reactive.Linq</Namespace>
</Query>

void Main()
{
	for(var index = 0; index < 3; ++index)
	{
		Fib(index).Dump();
	}
}

(int a, int c) Fib(int n)
{
	(int a, int b, int c) args = (0, 1, 0);

	for(var i = 0; i < n; ++i)
	{
		args = (args.b, args.b + args.a, args.a);
	}
	
	return (args.a, args.c);
}