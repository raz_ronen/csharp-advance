<Query Kind="Program">
  <NuGetReference>System.Reactive</NuGetReference>
  <Namespace>System.Reactive</Namespace>
  <Namespace>System.Reactive.Linq</Namespace>
</Query>

void Main()
{
	for (var index = 0; index < 10; ++index)
	{
		Fib(index).Dump();
	}
	
	int Fib(int a)
	{
		if(a < 2)
		{
			return a;
		}
		
		return Fib(a - 1) + Fib(a - 2);
	}	
}