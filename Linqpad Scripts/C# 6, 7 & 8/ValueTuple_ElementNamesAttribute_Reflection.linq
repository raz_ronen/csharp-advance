<Query Kind="Program">
  <NuGetReference>System.Reactive</NuGetReference>
  <Namespace>System.Reactive</Namespace>
  <Namespace>System.Reactive.Linq</Namespace>
  <Namespace>System.Runtime.CompilerServices</Namespace>
</Query>

void Main()
{
		var arr = new []{ 1D, 2D, 3D, 4D, };
		
		var (first, rest) = arr.Tally();
		
		Type t = typeof(Extensions);
		MethodInfo method = t.GetMethod(nameof(Extensions.Tally));
		var attr = method.ReturnParameter.GetCustomAttribute<TupleElementNamesAttribute>();
	
		IList<string> names = attr.TransformNames;
		
		names.Dump();
}

public static class Extensions
{
	public static (int count, double sum) Tally(this IEnumerable<double> values)
	{
		int count = 0;
		double sum = 0.0;
		foreach (var value in values)
		{
			count++;
			sum += value;
		}
		return (count, sum);
	}
}