<Query Kind="Program">
  <NuGetReference>System.Reactive</NuGetReference>
  <Namespace>System.Reactive</Namespace>
  <Namespace>System.Reactive.Linq</Namespace>
</Query>

void Main()
{
	var point = new ReadonlyPoint3D(1, 2, 3);
	
	point.Dump();
	
	point.SideEffect();
	
	point.Dump();
}

public struct ReadonlyPoint3D
{
	public double X { get; }
	public double Y { get; }
	public double Z { get; }

	public ReadonlyPoint3D(double x, double y, double z) 
	{
		X = x;
		Y = y; 
		Z = z; 
	}
	
	public void SideEffect()
	{
		this = new ReadonlyPoint3D(666, 666, 666);
	}
}