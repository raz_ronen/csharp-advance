<Query Kind="Program">
  <NuGetReference>System.Reactive</NuGetReference>
  <Namespace>System.Reactive</Namespace>
  <Namespace>System.Reactive.Linq</Namespace>
</Query>

void Main()
{
		var arr = new []{ 1, 2, 3, 4, };
		
		var (first, rest) = arr;
		
		first.Dump();
		arr.Dump();
}

public static class Extensions
{
	public static void Deconstruct<T>(this T[] arr, out T first, out T[] rest)		
	{
		first = arr.First();
		rest = arr.Skip(1).ToArray();
	}

	public static void Deconstruct<T>(this T[] arr, out T first, out T second, out T[] rest)
	{
		first = arr[0];
		second = arr[1];
		rest = arr.Skip(2).ToArray();
	}
}