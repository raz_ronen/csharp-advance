<Query Kind="Program">
  <NuGetReference>System.Reactive</NuGetReference>
  <Namespace>System.Reactive</Namespace>
  <Namespace>System.Reactive.Linq</Namespace>
</Query>

void Main()
{
	var root = new Point(0, 0);
	var (x, y) = root;
	//or
	(x, y) = new Point(1, 1);

}

public class Point
{
	public double X { get; }
	public double Y { get; }
	
	public Point(double x, double y)
		=> (X, Y) = (x, y);

	public void Deconstruct(out double x, out double y)
	{
		x = X;
		y = Y; 
	}
	
	//public void Deconstruct(out double x, out double y) => (x, y) = (X, Y)
}