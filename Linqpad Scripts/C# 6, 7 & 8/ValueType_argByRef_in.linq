<Query Kind="Program">
  <NuGetReference>System.Reactive</NuGetReference>
  <Namespace>System.Reactive</Namespace>
  <Namespace>System.Reactive.Linq</Namespace>
</Query>

void Main()
{
	var point = new ReadonlyPoint3D(1, 2, 3);

	point.Dump();
	var result = CalculateDistance3(ref point);
	point.Dump();
}

private static double CalculateDistance3(ref ReadonlyPoint3D point1, ReadonlyPoint3D point2 = default)
{
	double xDifference = point1.X - point2.X;
	double yDifference = point1.Y - point2.Y;
	double zDifference = point1.Z - point2.Z;
	
	point1.X = 3;
	
	return Math.Sqrt(xDifference * xDifference + yDifference * yDifference + zDifference * zDifference); 
}


public struct ReadonlyPoint3D
{
	public double X { get; set; }
	public double Y { get; }
	public double Z { get; }

	public ReadonlyPoint3D(double x, double y, double z)
	{
		X = x;
		Y = y;
		Z = z;
	}
}