<Query Kind="Program">
  <NuGetReference>System.Reactive</NuGetReference>
  <Namespace>System.Reactive</Namespace>
  <Namespace>System.Reactive.Linq</Namespace>
</Query>

void Main()
{
	var (day, month, year) = DateTime.Now;
	
	(day, month, year).Dump();
}

public static class Extensions
{
	public static void Deconstruct(this DateTime datetime, out int day, out int month, out int year)
	{
		day = datetime.Day;
		month = datetime.Month;
		year = datetime.Year;
	}
}