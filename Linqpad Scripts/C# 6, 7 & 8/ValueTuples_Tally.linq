<Query Kind="Program">
  <NuGetReference>System.Reactive</NuGetReference>
  <Namespace>System.Reactive</Namespace>
  <Namespace>System.Reactive.Linq</Namespace>
</Query>

void Main()
{
	var values = new[] { 1.0, 2, 3, 4, 5, 6, 7, 8 };
	
	var tuple = Tally(values);
	Console.WriteLine($"There are {tuple.count} values and their sum is {tuple.sum}");
	
	(int count, double sum) = Tally(values);	
	Console.WriteLine($"There are {count} values and their sum is {sum}");
}

static (int count, double sum) Tally(IEnumerable<double> values)
{
	int count = 0;
	double sum = 0.0;
	foreach (var value in values)
	{
		count++;
		sum += value;
	}
	return (count, sum);
}