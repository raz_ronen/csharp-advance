<Query Kind="Program">
  <NuGetReference>System.Reactive</NuGetReference>
  <Namespace>System.Reactive</Namespace>
  <Namespace>System.Reactive.Linq</Namespace>
  <Namespace>System.Net</Namespace>
</Query>

void Main()
{
	var client = new WebClient();
	try
	{
		var response = client.DownloadString(new Uri("http://www.NotExistURL.com"));
	} 
	catch(WebException ex) when (ex.Status == WebExceptionStatus.NameResolutionFailure)	
	{
		Console.WriteLine("WebException - NameResolutionFailure");
	}
	catch (WebException ex) when (ex.Status == WebExceptionStatus.ConnectFailure)
	{
		Console.WriteLine("WebException - ConnectFailure");
	}
	catch(Exception ex)
	{
		Console.WriteLine("Exception");
	}
	finally
	{
		Console.WriteLine("Finally");
	}
}



// Define other methods and classes here
