<Query Kind="Program">
  <NuGetReference>System.Reactive</NuGetReference>
  <Namespace>System.Reactive</Namespace>
  <Namespace>System.Reactive.Linq</Namespace>
</Query>

//Also show in Sharplab
void Main()
{
	Shape shape = new Rectangle { Height = 20, Length = 20 };
	Test(shape);
	((Rectangle)shape).Height = 10;
	Test(shape);
	shape = new Circle() { Radius = 5 };
	Test(shape);
	shape = new Triangle();
	Test(shape);
	shape = null;
	Test(shape);
}

void Test(Shape shape)
{
	switch (shape) // Switch on anything
	{
		case Rectangle s when (s.Length == s.Height): // when-condition
			Console.WriteLine($"{s.Length} x {s.Height} square");
			break;
		case Rectangle r:
			Console.WriteLine($"{r.Length} x {r.Height} rectangle");
			break;
		case Circle cc when (cc.Radius > 10):
			Console.WriteLine($"A circle with a big radius {cc.Radius}");
			break;
		case Circle c: // Type pattern
			Console.WriteLine($"circle with radius {c.Radius}"); // use c
			break;
		case null:
			throw new ArgumentNullException(nameof(shape));
		default:
			Console.WriteLine("<unknown shape>");
			break;
	}
}

public abstract class Shape 
{ 
	
}

public class Rectangle : Shape
{
	public int Length { get; set; }
	public int Height { get; set; }
}

public class Circle : Shape
{
	public int Radius { get; set; }
}

public class Triangle : Shape 
{
	
}