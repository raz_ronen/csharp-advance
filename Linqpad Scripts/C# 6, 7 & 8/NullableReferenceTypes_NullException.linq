<Query Kind="Program">
  <NuGetReference>System.Reactive</NuGetReference>
  <Namespace>System.Reactive</Namespace>
  <Namespace>System.Reactive.Linq</Namespace>
</Query>

void Main()
{
	var person = new Person("Moaid", "Hathot");
	
	var result = Foo(person);
	
	Console.WriteLine($"The result of is {result}");
}

static int Foo(Person person)
	=> person.MiddleName.Length;

public class Person
{
	public string FirstName { get; }
	public string MiddleName { get; }
	public string LastName { get; }
	
	public Person(string first, string middle, string last)
	{
		FirstName = first;
		MiddleName = middle;
		LastName = last;
	}
	
	public Person(string first, string last)
		: this(first, null, last)		
	{
		
	}
}
