<Query Kind="Program">
  <NuGetReference>System.Reactive</NuGetReference>
  <Namespace>System.Reactive</Namespace>
  <Namespace>System.Reactive.Linq</Namespace>
</Query>

//Show in Sharplab

void Main()
{
	Action f = null;
	for (var index = 0; index < 10; ++index)
	{
		if (index == 0)
		{
			f = () => Console.WriteLine($"My Value is {index}");
		}
	}

	f();	
}