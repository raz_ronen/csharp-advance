<Query Kind="Program">
  <NuGetReference>System.Reactive</NuGetReference>
  <Namespace>System.Reactive</Namespace>
  <Namespace>System.Reactive.Linq</Namespace>
</Query>

void Main()
{
	for(var index = 0; index < 10; ++index)
	{
		Fib(index).Dump();
	}
}

int Fib(int n)
{
	//	int a = 0, b = 1;
	var (a, b) = (0, 1);

	for(var i = 0; i < n; ++i)
	{
		(a, b) = (b, a + b);
	}
	
	return a;
}