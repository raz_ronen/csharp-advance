<Query Kind="Program">
  <NuGetReference>System.Reactive</NuGetReference>
  <Namespace>System.Reactive</Namespace>
  <Namespace>System.Reactive.Linq</Namespace>
</Query>

void Main()
{
	bool flag = true;
	var sb = new StringBuilder();

	sb.Append("Foo " + flag == false + " Bar");
	Console.WriteLine(sb.ToString());
}