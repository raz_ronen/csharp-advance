<Query Kind="Program">
  <NuGetReference>System.Reactive</NuGetReference>
  <Namespace>System.Reactive</Namespace>
  <Namespace>System.Reactive.Linq</Namespace>
</Query>

void Main()
{
	Base b = new Derived();
	b.Foo();
}

public class Base
{
	public virtual void Foo(int i = 10)
	{
		Console.WriteLine("I'm Base. The number is {0}!", i);
	}
}

public class Derived : Base
{
	public override void Foo(int i = 999)
	{
		Console.WriteLine("I'm Derived!!!!. The number is {0}!", i);
	}
}
