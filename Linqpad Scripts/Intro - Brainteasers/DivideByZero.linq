<Query Kind="Program">
  <NuGetReference>System.Reactive</NuGetReference>
  <Namespace>System.Reactive</Namespace>
  <Namespace>System.Reactive.Linq</Namespace>
</Query>

void Main()
{
	double d1 = 5;
	double d2 = 0;

	try
	{
		double d3 = d1 / d2;
		Console.WriteLine($"Success");
	}
	catch (Exception)
	{
		Console.WriteLine("Fail");
		throw;
	}

	Console.WriteLine("Bye Bye");
}
