<Query Kind="Program">
  <NuGetReference>Newtonsoft.Json</NuGetReference>
  <Namespace>Newtonsoft.Json</Namespace>
  <Namespace>Newtonsoft.Json.Linq</Namespace>
  <Namespace>System.Threading.Tasks</Namespace>
</Query>

void Main()
{
	Parallel.ForEach(
		source: Enumerable.Range(0, 10),
		parallelOptions: new ParallelOptions { MaxDegreeOfParallelism = 2 },
		body: source => { Thread.Sleep(TimeSpan.FromSeconds(3)); Console.WriteLine(source); }
	);
}
