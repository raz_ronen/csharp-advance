<Query Kind="Program">
  <NuGetReference>Newtonsoft.Json</NuGetReference>
  <Namespace>Newtonsoft.Json</Namespace>
  <Namespace>Newtonsoft.Json.Linq</Namespace>
  <Namespace>System.Threading.Tasks</Namespace>
</Query>

void Main()
{
	Parallel.For(
		fromInclusive: 0, 
		toExclusive: 10,
		parallelOptions: new ParallelOptions { MaxDegreeOfParallelism = 2 },
		body: index => { Thread.Sleep(TimeSpan.FromSeconds(3)); Console.WriteLine(index); }
	);
}
