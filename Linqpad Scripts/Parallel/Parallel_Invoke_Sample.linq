<Query Kind="Program">
  <NuGetReference>Newtonsoft.Json</NuGetReference>
  <Namespace>Newtonsoft.Json</Namespace>
  <Namespace>Newtonsoft.Json.Linq</Namespace>
  <Namespace>System.Threading.Tasks</Namespace>
</Query>

void Main()
{
	Parallel.Invoke(
		new ParallelOptions { MaxDegreeOfParallelism = 2 },
		() => { Thread.Sleep(1000); Console.WriteLine(1); },
		() => { Thread.Sleep(1000); Console.WriteLine(2); },
		() => { Thread.Sleep(1000); Console.WriteLine(3); },
		() => { Thread.Sleep(1000); Console.WriteLine(4); },
		() => { Thread.Sleep(1000); Console.WriteLine(5); },
		() => { Thread.Sleep(1000); Console.WriteLine(6); },
		() => { Thread.Sleep(1000); Console.WriteLine(7); });

	Console.WriteLine(8);
}
