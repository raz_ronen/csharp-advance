﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CustomersApp {
	class AnotherCustomerComparer : IComparer<Customer> {
		#region IComparer<Customer> Members

		public int Compare(Customer x, Customer y) {
			return x.ID.CompareTo(y.ID);
		}

		#endregion
	}


	class Program {
		static void Main(string[] args) {
			Customer[] customers2 = {
				new Customer { ID = 123, Name = "Bart Simpson", Address = "Springfield" },
				new Customer { ID = 422, Name = "Peter Parker", Address = "New York" },
				new Customer { ID = 871, Name = "Clark Kent", Address = "Smallville" },
				new Customer { ID = 562, Name = "Homer Simpson", Address = "Springfield" }
			};

			List<Customer> customers = new List<Customer>(customers2);

			Console.WriteLine("original:");
			foreach(Customer c in customers)
				Console.WriteLine(c);

			Array.Sort(customers2);
			customers.Sort();

			Console.WriteLine("sorted:");
			foreach(Customer c in customers)
				Console.WriteLine(c);

			Console.WriteLine("Sorted with a custom comparer");
			Array.Sort(customers2, new AnotherCustomerComparer());
			customers.Sort(new AnotherCustomerComparer());

			foreach(Customer c in customers)
				Console.WriteLine(c);

		}
	}

}
