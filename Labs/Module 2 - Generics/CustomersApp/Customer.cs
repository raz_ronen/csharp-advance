﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CustomersApp {
	class Customer : IComparable<Customer>, IEquatable<Customer>, IEquatable<string> {
		public string Name { get; set; }
		public int ID { get; set; }
		public string Address { get; set; }

		#region IComparable<Customer> Members

		public int CompareTo(Customer other) {
			return string.Compare(Name, other.Name, true);
		}

		#endregion

		#region IEquatable<Customer> Members

		public bool Equals(Customer other) {
			return Name == other.Name && ID == other.ID;
		}

		#endregion

		public override bool Equals(object obj) {
			return Equals((Customer)obj);
		}

		public override int GetHashCode() {
			return Name.GetHashCode() ^ ID.GetHashCode();
		}
		public override string ToString() {
			return string.Format("{0}: {1}", ID, Name);
		}


		public bool Equals(string other) {
			return Name.Equals(other);
		}
	}
}
