﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Reflection;

namespace XLinq {
	class Program {
		static void Main(string[] args) {
			// ex. 2
			var types = from t in typeof(string).Assembly.GetExportedTypes()
							where t.IsClass
							let props = t.GetProperties()
							select new XElement("Type",
								new XAttribute("FullName", t.FullName),
								new XElement("Properties",
									from p in props
									select new XElement("Property",
										new XAttribute("Name", p.Name),
										new XAttribute("Type", p.PropertyType.FullName ?? "T"))),
								new XElement("Methods",
									from m in t.GetMethods(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly)
									where !m.IsSpecialName
									select new XElement("Method",
										new XAttribute("Name", m.Name),
										new XAttribute("ReturnType", m.ReturnType.FullName ?? "T"),
										new XElement("Parameters",
											from prm in m.GetParameters()
											select new XElement("Parameter",
												new XAttribute("Name", prm.Name),
												new XAttribute("Type", prm.ParameterType))))));
			var xmltypes = new XElement("Types", types);
			//Console.WriteLine(xmltypes);

			// ex. 3a
			var noprops = from t in types
							  where t.Element("Properties").Descendants().Count() == 0
							  let name = (string)t.Attribute("FullName")
							  orderby name
							  select name;
			Console.WriteLine("Types with no properties: {0}", noprops.Count());
			foreach(var p in noprops)
				Console.WriteLine(p);

			// ex. 3b
			Console.WriteLine("Total methods: {0}", types.Sum(t => t.Descendants("Method").Count()));

			// ex. 3c
			Console.WriteLine("Total properties: {0}", types.Sum(e => e.Descendants("Property").Count()));
			var parameters = from e in types.Descendants("Parameter")
								  group e
								  by (string)e.Attribute("Type")
									  into g
									  orderby g.Count() descending
									  select new {
										  Name = g.Key,
										  Count = g.Count()
									  };
			Console.WriteLine("Most common parameter type: {0} ({1} times)", parameters.First().Name, parameters.First().Count);

			// ex. 3d

			var propsAndMethods = from t in types
										 let methods = t.Descendants("Method").Count()
										 orderby methods descending
										 select new {
											 Name = (string)t.Attribute("FullName"),
											 Methods = methods,
											 Properties = t.Descendants("Property").Count()
										 };
			foreach(var i in propsAndMethods.Take(10))
				Console.WriteLine(i);

			// ex. 3e
			var typesByMethods = from t in types
										let methods = t.Descendants("Method").Count()
										orderby (string)t.Attribute("FullName")
										group new {
											Methods = methods,
											Properties = t.Descendants("Property").Count(),
											Name = (string)t.Attribute("FullName")
										} by methods
											into g
											orderby g.Key descending
											select g;
			foreach(var g in typesByMethods) {
				Console.WriteLine("Methods: {0}", g.Key);
				foreach(var i in g)
					Console.WriteLine("{0} ({1} Properties)", i.Name, i.Properties);
			}
		}
	}
}
