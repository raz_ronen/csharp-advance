﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Diagnostics;

namespace LinqExercise {
	class Program {
		static void Main(string[] args) {
			// EX 1a
			var types = from t in typeof(string).Assembly.GetExportedTypes()
							where t.IsInterface
							orderby t.Name
							select new {
								TypeName = t.Name,
								NumMethods = t.GetMethods().Length
							};

			foreach(var i in types)
				Console.WriteLine(i);
			Console.WriteLine();

			// EX 1b
			var processes = from p in Process.GetProcesses()
								 where p.CanAccess() && p.Threads.Count < 5
								 orderby p.Id
								 select new {
								    Name = p.ProcessName,
								    ID = p.Id,
								    Start = p.StartTime
								 };

			foreach(var i in processes)
				Console.WriteLine(i);
			Console.WriteLine();

			// EX 1c
			var processes2 = from p in Process.GetProcesses()
								  where p.Threads.Count < 15
								  orderby p.Id
								  group new {
									  Name = p.ProcessName,
									  ID = p.Id,
									  Threads = p.Threads.Count
								  } by p.BasePriority 
								  into g
								  orderby g.Key
								  select g;

			foreach(var g in processes2) {
			   Console.WriteLine("Priority: {0}", g.Key);
				foreach(var i in g)
				   Console.WriteLine(i);
			}

			// EX 1d
			Console.WriteLine("Total threads: {0}", Process.GetProcesses().Sum(
				p => p.Threads.Count));

			// Ex 2
			Customer c = new Customer { Age = 10 };
			Person p1 = new Person { Address = "Springfield", Age = 20 };
			Console.WriteLine(c);
			p1.CopyTo(c);

			Console.WriteLine(c);
		}
	}

	public static class Extensions {
		public static bool CanAccess(this Process p) {
			try {
				return p.Handle != IntPtr.Zero;
			}
			catch {
				return false;
			}
		}

		public static void CopyTo(this object src, object dst) {
			var props = from p in src.GetType().GetProperties()
							from q in dst.GetType().GetProperties()
							where p.Name == q.Name && p.CanRead && q.CanWrite && 
								p.PropertyType == q.PropertyType
							select new {
								SrcProperty = p,
								DstProperty = q
							};
			foreach(var p in props)
				p.DstProperty.SetValue(dst, p.SrcProperty.GetValue(src, null), null);

		}
	}
}
