﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LinqExercise {
	class Customer {
		public int ID { get; set; }
		public string Address { get; set; }
		public int Age { get; set; }
		public string Department { get; set; }

		public override string ToString() {
			return string.Format("ID: {0} lives in {1}, Aged {2}", ID, Address, Age);
		}
	}

	class Person {
		public string Address { get; set; }
		public int Age { get; set; }
		public string Name { get; set; }
	}

}
