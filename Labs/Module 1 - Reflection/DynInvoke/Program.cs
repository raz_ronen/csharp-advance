using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;

namespace DynInvoke {
	class Program {
		static void Main(string[] args) {
			object[] objects = { new A(), new B(), new C() };

			foreach(object o in objects)
				Console.WriteLine(InvokeHello(o, "Pavel"));
		}

		static string InvokeHello(object inst, string name) {
			//return (string)inst.GetType().InvokeMember("Hello", BindingFlags.Instance | 
			//BindingFlags.Public | BindingFlags.InvokeMethod,
			//	null, inst, new object[] { name });

			MethodInfo mi = inst.GetType().GetMethod("Hello",
				new Type[] { typeof(string) });
			return (string)mi.Invoke(inst, new object[] { name });
		}
	}
}
