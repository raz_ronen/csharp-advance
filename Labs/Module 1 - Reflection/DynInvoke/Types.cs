using System;
using System.Collections.Generic;
using System.Text;

namespace DynInvoke {
	class A {
		public string Hello(string name) {
			return "Hello " + name;
		}
	}

	class B {
		public string Hello(string name) {
			return "Bonjour " + name;
		}
	}

	class C {
		public string Hello(string name) {
			return "Nihau " + name;
		}
	}
}
