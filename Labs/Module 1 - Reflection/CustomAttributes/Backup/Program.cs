using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;

namespace CustomAttributes {
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct, AllowMultiple = true)]
	public class CodeReviewAttribute : Attribute {
		private readonly string _name;
		public string Name {
			get { return _name; }
		}

		private readonly string _reviewDate;
		public string ReviewDate {
			get { return _reviewDate; }
		}

		private readonly bool _approved;
		public bool Approved {
			get { return _approved; }
		}

		public CodeReviewAttribute(string name, string date, bool approved) {
			_name = name; _reviewDate = date; _approved = approved;
		}

	}

	class Program {
		static void Main(string[] args) {
			Console.WriteLine("All Code Approved: {0}",
				AnalyzeAssembly(Assembly.GetExecutingAssembly()));
			//AnalyzeAssembly(typeof(string).Assembly);
		}

		static bool AnalyzeAssembly(Assembly asm) {
			bool approved = true;
			foreach(Type t in asm.GetTypes()) {
				object[] attrs = t.GetCustomAttributes(typeof(CodeReviewAttribute), false);
				Console.WriteLine("Code review for type {0} ({1} reviewers)", t.Name, attrs.Length);
				foreach(CodeReviewAttribute cra in attrs) {
					Console.WriteLine("Code Review: Name: {0}, Date: {1}, Approved: {2}",
						cra.Name, cra.ReviewDate, cra.Approved);
					approved &= cra.Approved;
				}
				Console.WriteLine();
			}
			return approved;
		}
	}

	#region Sample Types

	[CodeReview("Peter Parker", "11/2/2007", true)]
	[CodeReview("Bart Simpson", "1/1/2008", false)]
	[CodeReview("Clark Kent", "1/1/2000", true)]
	[Serializable]
	class A {
	}

	[CodeReview("Donald Duck", "1/3/2002", true)]
	struct B {
	}

	[CodeReview("Winnie the Pooh", "3/12/2005", false)]
	[CodeReview("Micky Mouse", "3/3/3000", true)]
	class C {
	}

	#endregion
}
