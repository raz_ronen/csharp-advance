using System;

namespace SomeBigLib {
	public class TestBig {
		public TestBig() {
			Console.WriteLine("TestBig ctor");
		}

		~TestBig() {
			Console.WriteLine("~TestBig");
		}

		public string Hello(string name) {
			return "Hello " + name;
		}
	}
}
