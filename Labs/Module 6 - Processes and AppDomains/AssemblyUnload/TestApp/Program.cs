using System;
using SomeBigLib;

namespace TestApp {
	class Program {
		static void Main(string[] args) {
			Console.WriteLine("Main AppDomain: {0}", 
				AppDomain.CurrentDomain.FriendlyName);
			AppDomain ad = AppDomain.CreateDomain("Other");

			ad.DoCallBack(() => {
				Console.WriteLine("Executing AppDomain: {0}", AppDomain.CurrentDomain.FriendlyName);
				TestBig t = new TestBig();
				Console.WriteLine(t.Hello("Pavel"));
			});

			AppDomain.Unload(ad);
		}
	}
}
