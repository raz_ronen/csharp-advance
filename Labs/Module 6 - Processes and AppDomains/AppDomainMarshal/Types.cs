﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AppDomainMarshal {
	public class A {
		public A() {
			Console.WriteLine("A ctor in {0}", AppDomain.CurrentDomain.FriendlyName);
		}
		~A() {
			Console.WriteLine("~A in {0}", AppDomain.CurrentDomain.FriendlyName);
		}
	}

	[Serializable]
	public class B {
		public B() {
			Console.WriteLine("B ctor in {0}", AppDomain.CurrentDomain.FriendlyName);
		}
		~B() {
			Console.WriteLine("~B in {0}", AppDomain.CurrentDomain.FriendlyName);
		}

		public string Hello(string name) {
			return string.Format("Hello {0} From AppDomain {1}", name, AppDomain.CurrentDomain.FriendlyName);
		}
	}

	public class C : MarshalByRefObject {
		public C() {
			Console.WriteLine("C ctor in {0}", AppDomain.CurrentDomain.FriendlyName);
		}
		~C() {
			Console.WriteLine("~C in {0}", AppDomain.CurrentDomain.FriendlyName);
		}

		public string Hello(string name) {
			return string.Format("Hello {0} From AppDomain {1}", name, AppDomain.CurrentDomain.FriendlyName);
		}
	}
}
