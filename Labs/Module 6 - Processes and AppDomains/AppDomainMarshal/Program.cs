﻿using System;
using System.Runtime.Remoting;
using System.Reflection;
using System.Threading;

namespace AppDomainMarshal {

	class Program {
		static void Main(string[] args) {
			AppDomain ad = AppDomain.CreateDomain("Other");

			string asmname = Assembly.GetExecutingAssembly().FullName;
			A a;
			try {
				a = (A)ad.CreateInstanceAndUnwrap(asmname, typeof(A).FullName);
			}
			catch(Exception e) {
				Console.WriteLine("Error: {0}", e.Message);
			}

			C c = (C)ad.CreateInstanceAndUnwrap(asmname, typeof(C).FullName);
			Console.WriteLine("C is proxy: {0}", RemotingServices.IsTransparentProxy(c));
			Console.WriteLine("type of c is {0}", c.GetType());
			Console.WriteLine(c.Hello("Pavel"));

			B b = (B)ad.CreateInstanceAndUnwrap(asmname, "AppDomainMarshal.B");
			Console.WriteLine("B is proxy: {0}", RemotingServices.IsTransparentProxy(b));
			Console.WriteLine(b.Hello("Pavel"));

			AppDomain.Unload(ad);
		}
	}
}
