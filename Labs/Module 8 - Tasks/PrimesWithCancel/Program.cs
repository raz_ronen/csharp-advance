﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Threading;

namespace Primes {
	class Program {
		static void Main(string[] args) {
			Console.WriteLine("Calculating...");
			var result = CalcPrimes(3, 30000000);
			Console.WriteLine("Returned {0} numbers", result.Count());
		}

		static IEnumerable<int> CalcPrimes(int first, int last) {
			var list = new List<int>(1024);
			var rnd = new Random();
			Parallel.For(first, last, (i, state) => {
				lock(rnd) {
					if(rnd.Next(10000000) == 0) {
						state.Stop();
						Console.WriteLine("Cancelled...");
						return;
					}
				}
				int limit = (int)Math.Sqrt(i);
				bool prime = true;
				for(int j = 2; j <= limit; j++)
					if(i % j == 0) {
						prime = false;
						break;
					}
				if(prime)
					lock(list)
						list.Add(i);
			});
			return list;
		}
	}
}
