﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace Primes {
	class Program {
		static void Main(string[] args) {
			Console.WriteLine("Starting with Deg: 1");
			var sw = Stopwatch.StartNew();
			var result = CalcPrimes(3, 10000000, 1);
			sw.Stop();
			Console.WriteLine("Total: {0}", result.Count);
			Console.WriteLine("Elapsed with deg 1: {0}", sw.ElapsedMilliseconds);
			Console.WriteLine("Starting with Deg: -1");
			sw.Restart();
			result = CalcPrimes(3, 10000000);
			sw.Stop();
			Console.WriteLine("Total: {0}", result.Count);
			Console.WriteLine("Elapsed with deg -1: {0}", sw.ElapsedMilliseconds);
		}

		static ICollection<int> CalcPrimes(int first, int last, int degree = -1) {
			var options = new ParallelOptions { MaxDegreeOfParallelism = degree };
			var list = new SortedSet<int>();
			Parallel.For(first, last + 1, options, i => {
				int limit = (int)Math.Sqrt(i);
				bool prime = true;
				for(int j = 2; j <= limit; j++)
					if(i % j == 0) {
						prime = false;
						break;
					}
				if(prime)
					lock(list)
						list.Add(i);
			});
			return list;
		}
	}
}
