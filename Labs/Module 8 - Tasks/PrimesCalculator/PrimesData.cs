﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Threading;

namespace PrimesCalculator {
	class PrimesData {
		private readonly int _first, _last;
		private readonly Action<IEnumerable<int>> _updater;
		private readonly Action _canceller;
		private readonly ISynchronizeInvoke _invoker;
		private readonly CancellationToken _ct;

		public int First { get { return _first; } }
		public int Last { get { return _last; } }

		public PrimesData(ISynchronizeInvoke invoker, Action<IEnumerable<int>> updater, 
			Action canceller, int first, int last, CancellationToken ct) {
			_updater = updater;
			_invoker = invoker;
			_canceller = canceller;
			_first = first;
			_last = last;
			_ct = ct;
		}

		internal void Calculate() {
			List<int> primes = new List<int>();
			for(int n = _first; n <= _last; n++) {
				if(_ct.IsCancellationRequested) {
					// cancel operation
					_invoker.Invoke(_canceller, null);
					return;
				}
				int limit = (int)Math.Sqrt(n);
				bool isprime = true;
				for(int i = 2; i <= limit; i++)
					if(n % i == 0) {
						isprime = false;
						break;
					}
				if(isprime)
					primes.Add(n);
			}
			_invoker.Invoke(_updater, new object[] { primes });
		}
	}
}
