﻿#define SYNC

using System;
using System.Diagnostics;
using System.Threading;
using System.IO;

namespace InterProcessSync {
	class Program {
		static void Main(string[] args) {
			using(Mutex mutex = new Mutex(false, "_SyncFileWrite")) {

				Console.WriteLine("Press Enter to start...");
				Console.ReadLine();

				Console.WriteLine("Writing...");

				for(int i = 0; i < 10000; i++) {
#if SYNC
					mutex.WaitOne();
#endif
					try {
						using(StreamWriter writer = new StreamWriter(@"c:\temp\test.txt", true)) {
							writer.WriteLine("Writing information from process " + Process.GetCurrentProcess().Id);
						}
					}
					catch(Exception e) {
						Console.WriteLine("Exception: " + e);
						return;
					}
					finally {
#if SYNC
						mutex.ReleaseMutex();
#endif
					}
				}

				Console.WriteLine("Done.");
			}
		}
	}
}
