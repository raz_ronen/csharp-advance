﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Queues {
	class Program {
		static void Main(string[] args) {
			LimitedQueue<int> queue = new LimitedQueue<int>(20);
			Timer t = new Timer((_) => {
				Console.WriteLine("Items in Queue: {0}", queue.Count);
			}, null, 50, 50);

			for(int i = 0; i < 100; i++)
				ThreadPool.QueueUserWorkItem((o) => {
					Random r = new Random();
					for(int j = 0; j < 100; j++) {
						if(r.Next(10) >= 6)
							queue.Enque(r.Next(100));
						else {
							try {
								queue.Deque();
							}
							catch {
							}
						}
						Thread.Sleep(r.Next(20));
					}
				});
			Thread.Sleep(10000);
			t.Dispose();
		}
	}
}
