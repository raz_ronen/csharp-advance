﻿#define USE_LOCK

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Queues {
	public class LimitedQueue<T> {
		private Semaphore _sem;
		private Queue<T> _queue;
		private object _lock = new object();

		public LimitedQueue(int size) {
			if(size < 1)
				throw new ArgumentException("size");
			_sem = new Semaphore(size, size);
			_queue = new Queue<T>(size);
		}

		public void Enque(T value) {
			_sem.WaitOne();
#if USE_LOCK
			lock(_lock)
#endif
				_queue.Enqueue(value);
		}

		public T Deque() {
			T result;
#if USE_LOCK
			lock(_lock) {
#endif
				result = _queue.Dequeue();
				_sem.Release();
#if USE_LOCK
			}
#endif
			return result;
		}

		public int Count {
			get { 
				return _queue.Count; 
			}
		}
	}
}
