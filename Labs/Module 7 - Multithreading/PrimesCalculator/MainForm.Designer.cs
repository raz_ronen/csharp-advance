﻿namespace PrimesCalculator {
	partial class MainForm {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if(disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			System.Windows.Forms.Label label1;
			System.Windows.Forms.Label label2;
			this.txtFirst = new System.Windows.Forms.TextBox();
			this.txtLast = new System.Windows.Forms.TextBox();
			this.cmdCalc = new System.Windows.Forms.Button();
			this.listBox1 = new System.Windows.Forms.ListBox();
			this.cmdCancel = new System.Windows.Forms.Button();
			label1 = new System.Windows.Forms.Label();
			label2 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// txtFirst
			// 
			this.txtFirst.Location = new System.Drawing.Point(12, 39);
			this.txtFirst.Name = "txtFirst";
			this.txtFirst.Size = new System.Drawing.Size(119, 20);
			this.txtFirst.TabIndex = 0;
			// 
			// txtLast
			// 
			this.txtLast.Location = new System.Drawing.Point(12, 70);
			this.txtLast.Name = "txtLast";
			this.txtLast.Size = new System.Drawing.Size(119, 20);
			this.txtLast.TabIndex = 1;
			// 
			// cmdCalc
			// 
			this.cmdCalc.Location = new System.Drawing.Point(12, 221);
			this.cmdCalc.Name = "cmdCalc";
			this.cmdCalc.Size = new System.Drawing.Size(119, 23);
			this.cmdCalc.TabIndex = 2;
			this.cmdCalc.Text = "&Calculate";
			this.cmdCalc.UseVisualStyleBackColor = true;
			this.cmdCalc.Click += new System.EventHandler(this.cmdCalc_Click);
			// 
			// listBox1
			// 
			this.listBox1.FormattingEnabled = true;
			this.listBox1.Location = new System.Drawing.Point(159, 39);
			this.listBox1.Name = "listBox1";
			this.listBox1.Size = new System.Drawing.Size(124, 238);
			this.listBox1.TabIndex = 3;
			// 
			// cmdCancel
			// 
			this.cmdCancel.Enabled = false;
			this.cmdCancel.Location = new System.Drawing.Point(12, 254);
			this.cmdCancel.Name = "cmdCancel";
			this.cmdCancel.Size = new System.Drawing.Size(119, 23);
			this.cmdCancel.TabIndex = 4;
			this.cmdCancel.Text = "Cancel";
			this.cmdCancel.UseVisualStyleBackColor = true;
			this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
			// 
			// label1
			// 
			label1.AutoSize = true;
			label1.Location = new System.Drawing.Point(156, 20);
			label1.Name = "label1";
			label1.Size = new System.Drawing.Size(45, 13);
			label1.TabIndex = 5;
			label1.Text = "Results:";
			// 
			// label2
			// 
			label2.AutoSize = true;
			label2.Location = new System.Drawing.Point(12, 20);
			label2.Name = "label2";
			label2.Size = new System.Drawing.Size(34, 13);
			label2.TabIndex = 6;
			label2.Text = "Input:";
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(308, 305);
			this.Controls.Add(label2);
			this.Controls.Add(label1);
			this.Controls.Add(this.cmdCancel);
			this.Controls.Add(this.listBox1);
			this.Controls.Add(this.cmdCalc);
			this.Controls.Add(this.txtLast);
			this.Controls.Add(this.txtFirst);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.MaximizeBox = false;
			this.Name = "MainForm";
			this.Text = "Primes Calculator";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TextBox txtFirst;
		private System.Windows.Forms.TextBox txtLast;
		private System.Windows.Forms.Button cmdCalc;
		private System.Windows.Forms.ListBox listBox1;
		private System.Windows.Forms.Button cmdCancel;
	}
}

