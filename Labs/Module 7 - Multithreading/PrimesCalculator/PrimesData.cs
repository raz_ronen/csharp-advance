﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Threading;

namespace PrimesCalculator {
	class PrimesData {
		private readonly int _first, _last;
		private readonly Action<IEnumerable<int>> _updater;
		private readonly Action _canceller;
		private readonly ISynchronizeInvoke _invoker;

		SynchronizationContext _sc;

		public int First { get { return _first; } }
		public int Last { get { return _last; } }

		public PrimesData(ISynchronizeInvoke invoker, Action<IEnumerable<int>> updater, 
			Action canceller, int first, int last) {
			_updater = updater;
			_invoker = invoker;
			_canceller = canceller;
			_first = first;
			_last = last;
			_sc = SynchronizationContext.Current;
		}

		internal void Calculate() {
			EventWaitHandle cancelEvent = new EventWaitHandle(false, EventResetMode.AutoReset, 
				"_CancelPrimeCalc");

			List<int> primes = new List<int>();
			for(int n = _first; n <= _last; n++) {
				if(cancelEvent.WaitOne(0)) {
					// cancel operation
					cancelEvent.Close();
					//_invoker.Invoke(_canceller, null);
					_sc.Send(delegate {
						_canceller();
					}, null);
					return;
				}
				int limit = (int)Math.Sqrt(n);
				bool isprime = true;
				for(int i = 2; i <= limit; i++)
					if(n % i == 0) {
						isprime = false;
						break;
					}
				if(isprime)
					primes.Add(n);
			}
			_sc.Send(delegate {
				_updater(primes);
			}, null);
			//_invoker.Invoke(_updater, new object[] { primes });
		}
	}
}
