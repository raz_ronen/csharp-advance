﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows.Forms;

namespace PrimesCalculator {
	public partial class MainForm : Form {
		private EventWaitHandle _cancelEvent;

		public MainForm() {
			InitializeComponent();
		}

		private void cmdCalc_Click(object sender, EventArgs e) {
			int first, last = 0;
			bool ok = int.TryParse(txtFirst.Text, out first) && 
				int.TryParse(txtLast.Text, out last);
			if(!ok) {
				MessageBox.Show("Please enter numbers only.");
				return;
			}

			cmdCalc.Enabled = false;
			listBox1.Items.Clear();
			listBox1.Items.Add("Calculating...");
			listBox1.Enabled = false;
			cmdCancel.Enabled = true;

			_cancelEvent = new EventWaitHandle(false, EventResetMode.AutoReset, "_CancelPrimeCalc");

			PrimesData data = new PrimesData(this, UpdateList, CancelCalc, first, last);
			Thread th = new Thread(data.Calculate);
			th.Start();
		}

		private void UpdateList(IEnumerable<int> list) {
			listBox1.Items.Clear();
			listBox1.BeginUpdate();
			foreach(int n in list)
				listBox1.Items.Add(n);
			listBox1.EndUpdate();
			listBox1.Enabled = cmdCalc.Enabled = true;
			cmdCancel.Enabled = false;
		}

		private void cmdCancel_Click(object sender, EventArgs e) {
			_cancelEvent.Set();
		}

		private void CancelCalc() {
			_cancelEvent.Close();

			listBox1.Enabled = cmdCalc.Enabled = true;
			cmdCancel.Enabled = false;
			listBox1.Items[0] = "Operation cancelled.";
		}
	}
}
