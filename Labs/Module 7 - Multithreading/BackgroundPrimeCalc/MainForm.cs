﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;

namespace BackgroundPrimeCalc {
	public partial class MainForm : Form {
		private class WorkData {
			public int First, Last;
		}

		public MainForm() {
			InitializeComponent();
		}

		private void _worker_DoWork(object sender, DoWorkEventArgs e) {
			WorkData data = e.Argument as WorkData;
			Debug.Assert(data != null);

			e.Result = CalcPrimes(data.First, data.Last);
		}

		private void button1_Click(object sender, EventArgs e) {
			WorkData data = new WorkData();
			data.First = int.Parse(textBox1.Text);
			data.Last = int.Parse(textBox2.Text);

			button1.Enabled = false;
			button2.Enabled = true;
			listBox1.DataSource = null;
			listBox1.Items.Clear();
			listBox1.Items.Add("Calculating...");
			_worker.RunWorkerAsync(data);
		}

		private IEnumerable<int> CalcPrimes(int first, int last) {
			List<int> list = new List<int>();
			int count = 0;
			for(int n = first; n <= last; n++) {
				if(_worker.CancellationPending)
					return null;
				bool prime = true;
				int limit = (int)Math.Sqrt(n);
				for(int i = 2; i <= limit; i++)
					if(n % i == 0) {
						prime = false;
						break;
					}
				if(prime)
					list.Add(n);
				if(++count % 100 == 0)
					_worker.ReportProgress((int)(100L * (n - first + 1) / (last - first + 1)));
			}
			_worker.ReportProgress(100);
			return list;
		}

		private void _worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
			listBox1.DataSource = e.Result as IEnumerable<int>;
			listBox1.Refresh();
			button1.Enabled = true;
			button2.Enabled = false;
		}

		private void button2_Click(object sender, EventArgs e) {
			_worker.CancelAsync();
		}

		private void _worker_ProgressChanged(object sender, ProgressChangedEventArgs e) {
			progressBar1.Value = e.ProgressPercentage;
		}
	}
}
