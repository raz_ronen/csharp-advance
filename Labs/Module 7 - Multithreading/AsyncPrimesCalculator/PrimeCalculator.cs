﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AsyncPrimesCalculator {
	public class PrimeCalculator {
		private delegate IEnumerable<int> PrimeDelegate(int first, int last);

		public IEnumerable<int> CalcPrimes(int start, int end) {
			return DoCalc(start, end);
		}

		private static Func<int, int, IEnumerable<int>> del = DoCalc;

		public IAsyncResult BeginCalcPrimes(int start, int end, AsyncCallback cb, object state) {
			return del.BeginInvoke(start, end, cb, state);
		}

		public IEnumerable<int> EndCalcPrimes(IAsyncResult call) {
			return del.EndInvoke(call);
		}

		private static IEnumerable<int> DoCalc(int start, int end) {
			List<int> primes = new List<int>();
			for(int n = start; n <= end; n++) {
				bool prime = true;
				int limit = (int)Math.Sqrt(n);
				for(int i = 2; i <= limit; i++)
					if(n % i == 0) {
						prime = false;
						break;
					}
				if(prime)
					primes.Add(n);
			}
			return primes;
		}
	}
}
