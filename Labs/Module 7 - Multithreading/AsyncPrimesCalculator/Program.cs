﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace AsyncPrimesCalculator {
	class Program {
		static void Main(string[] args) {
			ThreadPool.QueueUserWorkItem(delegate {
				Console.WriteLine("Thread: {0}", Thread.CurrentThread.ManagedThreadId);
			});

			PrimeCalculator calc = new PrimeCalculator();
			foreach(int n in calc.CalcPrimes(3, 1000))
				Console.Write(n + "\t");

			Console.WriteLine();

			AutoResetEvent e = new AutoResetEvent(false);

			IAsyncResult call = calc.BeginCalcPrimes(3, 2000, delegate(IAsyncResult ar) {
				Console.WriteLine("Calculated in thread: {0}", Thread.CurrentThread.ManagedThreadId);
				foreach(int n in calc.EndCalcPrimes(ar))
					Console.Write(n + "\t");
				Console.WriteLine();
				e.Set();
			}, null);

			Console.WriteLine("Doing something else... Thread={0}", Thread.CurrentThread.ManagedThreadId);
			e.WaitOne();
		}
	}
}
