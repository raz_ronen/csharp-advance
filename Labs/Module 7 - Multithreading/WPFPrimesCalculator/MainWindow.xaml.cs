﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;

namespace WPFPrimesCalculator {
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window {
		public MainWindow() {
			InitializeComponent();
		}

		private void OnCancelCalculate(object sender, RoutedEventArgs e) {
			using(var ev = new EventWaitHandle(false, EventResetMode.AutoReset, "_CancelEvent")) {
				ev.Set();
			}
		}

		private void OnCalculate(object sender, RoutedEventArgs e) {
			_calc.IsEnabled = false;
			_cancel.IsEnabled = true;
			int first = First, last = Last;
			Primes = null;
			_pbar.Maximum = last - first;
			Progress = 0;
			Thread t = new Thread(() => {
				var list = new List<int>();
				using(var ev = new EventWaitHandle(false, EventResetMode.AutoReset, "_CancelEvent")) {
					int count = 0;
					for(int i = first; i <= last; i++) {
						if(ev.WaitOne(0))
							break;
						int limit = (int)Math.Sqrt(i);
						bool isprime = true;
						for(int j = 2; j <= limit; j++)
							if(i % j == 0) {
								isprime = false;
								break;
							}
						if(isprime)
							list.Add(i);
						// update progress
						if(++count % 500 == 0)
							Dispatcher.BeginInvoke(new Action(() => {
								Progress += 500;
							}));
					}
				}
				Dispatcher.Invoke(new Action(() => {
					Primes = list;
					Progress = (int)_pbar.Maximum;
					_calc.IsEnabled = true;
					_cancel.IsEnabled = false;
				}));
			});
			t.IsBackground = true;
			t.Start();
		}

		public int First {
			get { return (int)GetValue(FirstProperty); }
			set { SetValue(FirstProperty, value); }
		}

		public int Last {
			get { return (int)GetValue(LastProperty); }
			set { SetValue(LastProperty, value); }
		}

		public IEnumerable<int> Primes {
			get { return (IEnumerable<int>)GetValue(PrimesProperty); }
			set { SetValue(PrimesProperty, value); }
		}

		public int Progress {
			get { return (int)GetValue(ProgressProperty); }
			set { SetValue(ProgressProperty, value); }
		}

		public static readonly DependencyProperty ProgressProperty =
			 DependencyProperty.Register("Progress", typeof(int), typeof(MainWindow), new UIPropertyMetadata(0));


		public static readonly DependencyProperty PrimesProperty =
			 DependencyProperty.Register("Primes", typeof(IEnumerable<int>), typeof(MainWindow), new UIPropertyMetadata(null));

		public static readonly DependencyProperty FirstProperty =
			 DependencyProperty.Register("First", typeof(int), typeof(MainWindow), new UIPropertyMetadata(2));

		public static readonly DependencyProperty LastProperty =
			 DependencyProperty.Register("Last", typeof(int), typeof(MainWindow), new UIPropertyMetadata(10));


	}
}
