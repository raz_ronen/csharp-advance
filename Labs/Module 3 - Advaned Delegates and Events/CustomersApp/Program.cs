﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CustomersApp {
	class AnotherCustomerComparer : IComparer<Customer> {
		#region IComparer<Customer> Members

		public int Compare(Customer x, Customer y) {
			return x.ID.CompareTo(y.ID);
		}

		#endregion
	}

	delegate bool CustomerFilter(Customer customer);

	class Program {
		static void Main(string[] args) {
			Customer[] customers = {
				new Customer { ID = 123, Name = "Bart Simpson", Address = "Springfield" },
				new Customer { ID = 422, Name = "Peter Parker", Address = "New York" },
				new Customer { ID = 871, Name = "Clark Kent", Address = "Smallville" },
				new Customer { ID = 562, Name = "Homer Simpson", Address = "Springfield" },
				new Customer { ID = 388, Name = "Sherlock Holmes", Address = "Baker street, London" }
			};

			Console.WriteLine("original:");
			foreach(Customer c in customers)
				Console.WriteLine(c);

			Console.WriteLine("Customers A-K");
			foreach(Customer c in GetCustomers(customers, IsCustomerA2K))
				Console.WriteLine(c);

			Console.WriteLine("Customers L-Z");
			CustomerFilter custL2Zfilter = delegate(Customer c) {
				return c.Name[0] >= 'L' && c.Name[0] <= 'Z';
			};
			foreach(Customer c in GetCustomers(customers, custL2Zfilter))
				Console.WriteLine(c);

			Console.WriteLine("ID less than 500");
			//CustomerFilter filter2 = c => c.ID < 500;
			foreach(Customer c in GetCustomers(customers, c => c.ID < 500))
				Console.WriteLine(c);
		}

		static bool IsCustomerA2K(Customer customer) {
			return customer.Name[0] >= 'A' && customer.Name[0] < 'L';
		}

		static IEnumerable<Customer> GetCustomers(IEnumerable<Customer> customers,
			CustomerFilter filter) {
			List<Customer> list = new List<Customer>();
			foreach(Customer customer in customers)
				if(filter(customer))
					list.Add(customer);
			return list;
		}
	}

}
