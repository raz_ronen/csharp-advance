using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace AsyncPrimes {
	public delegate IEnumerable<int> CollectionDelegate(int n1, int n2);
//	internal delegate void UpdateListDelegate(IEnumerable<int> list);

	public partial class Form1 : Form {
		public Form1() {
			InitializeComponent();
		}

		private void button1_Click(object sender, EventArgs e) {
			CollectionDelegate del = CalcPrimes;
			button1.Enabled = false;
			listBox1.Items.Clear();
			listBox1.Items.Add("Calculating...");

			#region Invoke asynchronously
			del.BeginInvoke(int.Parse(textBox1.Text), int.Parse(textBox2.Text),
				delegate(IAsyncResult result) {
					IEnumerable<int> list = del.EndInvoke(result);
					//listBox1.DataSource = list;
					//listBox1.Refresh();		
					
					this.Invoke(new Action<IEnumerable<int>>(UpdateList), list);

				}, null);
			#endregion
		}

		private static IEnumerable<int> CalcPrimes(int from, int to) {
			List<int> primes = new List<int>();
			for(int i = from; i <= to; i++) {
				int limit = (int)Math.Sqrt(i);
				bool isprime = true;
				for(int j = 2; j <= limit; j++)
					if(i % j == 0) {
						isprime = false;
						break;
					}
				if(isprime)
					primes.Add(i);
			}
			return primes;
		}

		private void UpdateList(IEnumerable<int> list) {
			listBox1.BeginUpdate();
			listBox1.Items.Clear();
			foreach(int n in list)
				listBox1.Items.Add(n);
			listBox1.EndUpdate();
			button1.Enabled = true;
		}
	}
}